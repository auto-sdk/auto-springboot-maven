package FILE_PACKAGE_NAME;

import UTIL_PACKAGE_NAME.BodyReaderHttpServletRequestWrapper;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CorsFilter implements Filter{

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
						 FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods","POST, GET, DELETE, PUT");
		response.addHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, authToken");
		response.setHeader("Access-Control-Max-Age", "1728000");

		if(ServletFileUpload.isMultipartContent(request)){
			filterChain.doFilter(request, response);
		}else{
			ServletRequest requestWrapper = new BodyReaderHttpServletRequestWrapper(request);
			filterChain.doFilter(requestWrapper, response);
		}
	}

	@Override
	public void destroy() {

	}
}
