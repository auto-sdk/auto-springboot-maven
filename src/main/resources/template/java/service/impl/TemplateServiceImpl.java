package FILE_PACKAGE_NAME;

import ENTITY_PACKAGE.UPPER_ENTITY_NAME;
import MAPPER_PACKAGE.UPPER_ENTITY_NAMEMapper;
import SERVICE_PAKCAGE.UPPER_ENTITY_NAMEService;
import UTILS_PACKAGE.ConstantUtil;
import UTILS_PACKAGE.StringUtil;
import UTILS_PACKAGE.RegexUtil;
import UTILS_PACKAGE.DaoException;
import UTILS_PACKAGE.ValidateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
UUID_PACKAGE

/**
 *  TABLE_COMMENT
 * 
 * @author PROJECT_AUTHOR
 * @version 1.0
 * @date NOW_DATE
 */
@Service
public class UPPER_ENTITY_NAMEServiceImpl implements UPPER_ENTITY_NAMEService {

	@Autowired
	private UPPER_ENTITY_NAMEMapper LOWER_ENTITY_NAMEMapper;

	/**
	 * 新增
	 * 
	 * @param LOWER_ENTITY_NAME
	 * @return
	 * @throws DaoException
	 */
	public int insertUPPER_ENTITY_NAME(UPPER_ENTITY_NAME LOWER_ENTITY_NAME) throws DaoException {
		int result = 0;
		try {
			if(StringUtil.isNull(LOWER_ENTITY_NAME.getUPPER_PRIMARY_KEY())){
				LOWER_ENTITY_NAME.setUPPER_PRIMARY_KEY(SET_PRIMARY_KEY);
			}
			result = LOWER_ENTITY_NAMEMapper.insertUPPER_ENTITY_NAME(LOWER_ENTITY_NAME);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_ADD_ERROR);
		}
		return result;
	}

	/**
	 * 修改
	 * 
	 * @param LOWER_ENTITY_NAME
	 * @return
	 * @throws DaoException
	 */
	public int updateUPPER_ENTITY_NAME(UPPER_ENTITY_NAME LOWER_ENTITY_NAME) throws DaoException {
		int result = 0;
		try {
			result = LOWER_ENTITY_NAMEMapper.updateUPPER_ENTITY_NAME(LOWER_ENTITY_NAME);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 修改ByMap
	 *
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int updateUPPER_ENTITY_NAMEByMap(Map<String, Object> map) throws DaoException{
		int result = 0;
		try {
			result = LOWER_ENTITY_NAMEMapper.updateUPPER_ENTITY_NAMEByMap(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_EDIT_ERROR);
		}
		return result;
	}

	/**
	 * 删除
	 *
	 * @param LOWER_PRIMARY_KEYs
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public int deleteUPPER_ENTITY_NAME(String ... LOWER_PRIMARY_KEYs) throws DaoException,ValidateException {
		int result = 0;

		if(StringUtil.isNull(LOWER_PRIMARY_KEYs)){
			throw new ValidateException(ConstantUtil.MSG_404);
		}

		try {
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("LOWER_PRIMARY_KEYs",StringUtil.array2CharStr(LOWER_PRIMARY_KEYs));
			result = LOWER_ENTITY_NAMEMapper.deleteUPPER_ENTITY_NAME(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_DEL_ERROR);
		}
		return result;
	}


	/**
	 * 查询单个
	 * 
	 * @param LOWER_PRIMARY_KEY
	 * @return
	 * @throws DaoException
	 */
	public UPPER_ENTITY_NAME selectUPPER_ENTITY_NAMEByUPPER_PRIMARY_KEY(String LOWER_PRIMARY_KEY) throws DaoException {
		UPPER_ENTITY_NAME LOWER_ENTITY_NAME = null;
		try {
			LOWER_ENTITY_NAME = LOWER_ENTITY_NAMEMapper.selectUPPER_ENTITY_NAMEByUPPER_PRIMARY_KEY(UPDATE_LOWER_PRIMARY_KEY);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return LOWER_ENTITY_NAME;
	}

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<UPPER_ENTITY_NAME> selectUPPER_ENTITY_NAMEList(Map<String, Object> map) throws DaoException, ValidateException {
		List<UPPER_ENTITY_NAME> LOWER_ENTITY_NAMEs = null;

		try {
			if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
				Integer index = Integer.parseInt(map.get("pageIndex").toString());
				Integer size = Integer.parseInt(map.get("pageSize").toString());
				Integer count = (index-1)*size;
				map.put("countIndex", count);
			}

		} catch (Exception e){
			throw new ValidateException(e, ConstantUtil.MSG_PAGE_DATA_ERROR);
		}

		try {
			LOWER_ENTITY_NAMEs = LOWER_ENTITY_NAMEMapper.selectUPPER_ENTITY_NAMEList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}

		return LOWER_ENTITY_NAMEs;
	}

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<UPPER_ENTITY_NAME> selectAllUPPER_ENTITY_NAMEList(Map<String, Object> map) throws DaoException {
		List<UPPER_ENTITY_NAME> LOWER_ENTITY_NAMEs = null;
		try {
			LOWER_ENTITY_NAMEs = LOWER_ENTITY_NAMEMapper.selectAllUPPER_ENTITY_NAMEList(map);
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return LOWER_ENTITY_NAMEs;
	}

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectUPPER_ENTITY_NAMEListCount(Map<String, Object> map) throws DaoException {
		int count = 0;
		try {
			Object obj = LOWER_ENTITY_NAMEMapper.selectUPPER_ENTITY_NAMEListCount(map);
			if(obj != null){
				count = Integer.parseInt(obj.toString());
			}
		} catch (Exception e){
			throw new DaoException(e, ConstantUtil.MSG_SELECT_ERROR);
		}
		return count;
	}

}