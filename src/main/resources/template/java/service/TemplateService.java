package FILE_PACKAGE_NAME;

import ENTITY_PACKAGE.UPPER_ENTITY_NAME;
import UTILS_PACKAGE.DaoException;
import UTILS_PACKAGE.ValidateException;

import java.util.List;
import java.util.Map;

/**
 * TABLE_COMMENT
 * 
 * @author PROJECT_AUTHOR
 * @version 1.0
 * @date NOW_DATE
 */
public interface UPPER_ENTITY_NAMEService {

	/**
	 * 新增
	 *
	 * @param LOWER_ENTITY_NAME
	 * @return
	 * @throws DaoException
	 */
	public int insertUPPER_ENTITY_NAME(UPPER_ENTITY_NAME LOWER_ENTITY_NAME) throws DaoException;

	/**
	 * 修改
	 * 
	 * @param LOWER_ENTITY_NAME
	 * @return
	 * @throws DaoException
	 */
	public int updateUPPER_ENTITY_NAME(UPPER_ENTITY_NAME LOWER_ENTITY_NAME) throws DaoException;

	/**
	 * 修改ByMap
	 *
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int updateUPPER_ENTITY_NAMEByMap(Map<String, Object> map) throws DaoException;

	/**
	 * 删除
	 *
	 * @param LOWER_PRIMARY_KEYs
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public int deleteUPPER_ENTITY_NAME(String ... LOWER_PRIMARY_KEYs) throws DaoException,ValidateException;

	/**
	 * 查询单个
	 * 
	 * @param LOWER_PRIMARY_KEY
	 * @return
	 * @throws DaoException
	 */
	public UPPER_ENTITY_NAME selectUPPER_ENTITY_NAMEByUPPER_PRIMARY_KEY(String LOWER_PRIMARY_KEY) throws DaoException;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public List<UPPER_ENTITY_NAME> selectUPPER_ENTITY_NAMEList(Map<String, Object> map) throws DaoException, ValidateException;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public List<UPPER_ENTITY_NAME> selectAllUPPER_ENTITY_NAMEList(Map<String, Object> map) throws DaoException;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 * @throws DaoException
	 */
	public int selectUPPER_ENTITY_NAMEListCount(Map<String, Object> map) throws DaoException;

}