package FILE_PACKAGE_NAME;

import ENTITY_PACKAGE.UPPER_ENTITY_NAME;

import java.util.List;
import java.util.Map;

/**
 * TABLE_COMMENT
 * 
 * @author PROJECT_AUTHOR
 * @version 1.0
 * @date NOW_DATE
 */
public interface UPPER_ENTITY_NAMEMapper {

	/**
	 * 新增
	 *
	 * @param LOWER_ENTITY_NAME
	 * @return
	 * @throws Exception
	 */
	public int insertUPPER_ENTITY_NAME(UPPER_ENTITY_NAME LOWER_ENTITY_NAME) throws Exception;

	/**
	 * 修改
	 * 
	 * @param LOWER_ENTITY_NAME
	 * @return
	 * @throws Exception
	 */
	public int updateUPPER_ENTITY_NAME(UPPER_ENTITY_NAME LOWER_ENTITY_NAME) throws Exception;

	/**
	 * 修改ByMap
	 *
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int updateUPPER_ENTITY_NAMEByMap(Map<String, Object> map) throws Exception;

	/**
	 * 删除
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public int deleteUPPER_ENTITY_NAME(Map<String, Object> map) throws Exception;

	/**
	 * 查询单个
	 * 
	 * @param LOWER_PRIMARY_KEY
	 * @return
	 */
	public UPPER_ENTITY_NAME selectUPPER_ENTITY_NAMEByUPPER_PRIMARY_KEY(PRIMARY_KEY_TYPE LOWER_PRIMARY_KEY) throws Exception;

	/**
	 * 分页查询
	 * 
	 * @param map
	 * @return
	 */
	public List<UPPER_ENTITY_NAME> selectUPPER_ENTITY_NAMEList(Map<String, Object> map) throws Exception;

	/**
	 * 查询所有
	 * 
	 * @param map
	 * @return
	 */
	public List<UPPER_ENTITY_NAME> selectAllUPPER_ENTITY_NAMEList(Map<String, Object> map) throws Exception;

	/**
	 * 查询总数
	 * 
	 * @param map
	 * @return
	 */
	public int selectUPPER_ENTITY_NAMEListCount(Map<String, Object> map) throws Exception;

}