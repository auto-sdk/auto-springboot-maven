package FILE_PACKAGE_NAME;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.io.PrintWriter;

import UTIL_PACKAGE.ResultEntity;
import UTIL_PACKAGE.ConstantUtil;

/**
 * 拦截器
 */
public class ManageInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
        
    	boolean flag = true;
        String token = request.getHeader(ConstantUtil.HEADER_PARAM_TOKEN);
        if(StringUtils.isBlank(token)){
            //进入登录页面
            output(response,new ResultEntity<Object>(ConstantUtil.CODE_403,"用户未登录").toString());
            return false;
        }

        return flag;
    }


    private void output(HttpServletResponse response,String msg){
        try{
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.print(msg);
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object obj, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object obj, Exception e) throws Exception {

    }
}
