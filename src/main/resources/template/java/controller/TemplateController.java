package FILE_PACKAGE_NAME;

import ENTITY_PACKAGE.UPPER_ENTITY_NAME;
import SERVICE_PAKCAGE.UPPER_ENTITY_NAMEService;
import UTILS_PACKAGE.ConstantUtil;
import UTILS_PACKAGE.ResultEntity;
import ANNOTATION_PACKAGE.SysLog;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * TABLE_COMMENT
 * 
 * @author PROJECT_AUTHOR
 * @version 1.0
 * @date NOW_DATE
 */
@Api(tags="TABLE_COMMENT")
@RequestMapping(value = "ACCESS_BASE_URL")
@RestController
public class UPPER_ENTITY_NAMEController extends BaseController {

	@Autowired
	private UPPER_ENTITY_NAMEService LOWER_ENTITY_NAMEService;

	/**
	 * 查询UPPER_ENTITY_NAME集合
	 */
	@ApiImplicitParams({
		PAGE_SELECT_MORE_CONDITION
		@ApiImplicitParam(name = "pageIndex",value = "当前页码",dataType = "int",paramType = "query",required = false),
		@ApiImplicitParam(name = "pageSize",value = "每页条数",dataType = "int",paramType = "query",required = false),
		@ApiImplicitParam(name = "sortName",value = "排序名称",dataType = "String",paramType = "query",required = false),
		@ApiImplicitParam(name = "sortOrder",value = "排序顺序（asc,desc）",dataType = "String",paramType = "query",required = false)
	})
	@ApiOperation(value="分页查询TABLE_COMMENT")
	@GetMapping(value = "/LOWER_ENTITY_NAMEs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultEntity<List<UPPER_ENTITY_NAME>> LOWER_ENTITY_NAMEList(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String,Object> map=initRequestMap(request);
		List<UPPER_ENTITY_NAME> LOWER_ENTITY_NAMEs = LOWER_ENTITY_NAMEService.selectUPPER_ENTITY_NAMEList(map);
		int total = LOWER_ENTITY_NAMEService.selectUPPER_ENTITY_NAMEListCount(map);
		return new ResultEntity<List<UPPER_ENTITY_NAME>>(ConstantUtil.CODE_200, LOWER_ENTITY_NAMEs, total);
	}

	/**
	 * 根据LOWER_PRIMARY_KEY查询UPPER_ENTITY_NAME
	 */
	@ApiOperation(value="查询单个TABLE_COMMENT")
	@GetMapping(value = "/LOWER_ENTITY_NAME/{LOWER_PRIMARY_KEY}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultEntity<UPPER_ENTITY_NAME> LOWER_ENTITY_NAMEByUPPER_PRIMARY_KEY(@PathVariable String LOWER_PRIMARY_KEY,
			 HttpServletRequest request, HttpServletResponse response) throws Exception {
		UPPER_ENTITY_NAME LOWER_ENTITY_NAME = LOWER_ENTITY_NAMEService.selectUPPER_ENTITY_NAMEByUPPER_PRIMARY_KEY(LOWER_PRIMARY_KEY);
		return new ResultEntity<UPPER_ENTITY_NAME>(ConstantUtil.CODE_200,LOWER_ENTITY_NAME);
	}

	/**
	 * 新增UPPER_ENTITY_NAME
	 */
	@ApiOperation(value="新增TABLE_COMMENT")
	@SysLog(operateType="新增TABLE_COMMENT")
	@PostMapping(value = "/LOWER_ENTITY_NAME",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultEntity addUPPER_ENTITY_NAME(@RequestBody UPPER_ENTITY_NAME LOWER_ENTITY_NAME, HttpServletRequest request, HttpServletResponse response) throws Exception{
		LOWER_ENTITY_NAMEService.insertUPPER_ENTITY_NAME(LOWER_ENTITY_NAME);
		return new ResultEntity(ConstantUtil.CODE_200,ConstantUtil.MSG_ADD_SUCCESS);
	}

	/**
	 * 修改UPPER_ENTITY_NAME
	 */
	@ApiOperation(value="修改TABLE_COMMENT")
	@SysLog(operateType="修改TABLE_COMMENT")
	@PutMapping(value = "/LOWER_ENTITY_NAME", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultEntity updateUPPER_ENTITY_NAME(@RequestBody UPPER_ENTITY_NAME LOWER_ENTITY_NAME,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		LOWER_ENTITY_NAMEService.updateUPPER_ENTITY_NAME(LOWER_ENTITY_NAME);
		return new ResultEntity(ConstantUtil.CODE_200,ConstantUtil.MSG_EDIT_SUCCESS);
	}

	/**
	 * 删除UPPER_ENTITY_NAME
	 */
	@ApiOperation(value="删除TABLE_COMMENT")
	@SysLog(operateType="删除TABLE_COMMENT")
	@DeleteMapping(value = "/LOWER_ENTITY_NAME/{LOWER_PRIMARY_KEY}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultEntity deleteUPPER_ENTITY_NAME(@PathVariable String LOWER_PRIMARY_KEY,
			HttpServletRequest request, HttpServletResponse response) throws Exception{
		LOWER_ENTITY_NAMEService.deleteUPPER_ENTITY_NAME(LOWER_PRIMARY_KEY);
		return new ResultEntity(ConstantUtil.CODE_200,ConstantUtil.MSG_DEL_SUCCESS);
	}

	/**
	 * 删除多个UPPER_ENTITY_NAME
	 */
	@ApiOperation(value="删除多个TABLE_COMMENT")
	@SysLog(operateType="删除多个TABLE_COMMENT")
	@DeleteMapping(value = "/LOWER_ENTITY_NAMEs", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResultEntity deleteUPPER_ENTITY_NAME(@RequestParam String [] LOWER_PRIMARY_KEYs,
												HttpServletRequest request, HttpServletResponse response) throws Exception{
		LOWER_ENTITY_NAMEService.deleteUPPER_ENTITY_NAME(LOWER_PRIMARY_KEYs);
		return new ResultEntity(ConstantUtil.CODE_200,ConstantUtil.MSG_DEL_SUCCESS);
	}
}