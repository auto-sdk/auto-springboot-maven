package FILE_PACKAGE_NAME;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 基础工具控制器
 * 
 */
@ApiIgnore
@Controller
public class BaseController {

	/**
	 * 初始化request
	 */
	protected Map<String, Object> initRequestMap(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String,String[]> params = request.getParameterMap();
		if(params != null) {
			for(Entry<String,String[]> entry:params.entrySet()){
				String key = entry.getKey();
				String [] value = entry.getValue();
				if(StringUtils.isNotEmpty(key)
					&& value != null && value.length > 0) {
					if(StringUtils.isNotEmpty(value[0])){
						map.put(key, value[0]);
					}
				}
			}
		}
		return map;
	}
	
}
