package FILE_PACKAGE_NAME;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger注册
 */
@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class Swagger2Config {

    @Bean
    public Docket createAPI() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo("PC"))
                .groupName("PC")
                .select()
                .apis(RequestHandlerSelectors.basePackage("CONTROLLER_PACKAGE"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo(String sysType) {

        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("PROJECT_NAME接口文档")
                .description(sysType+"接口测试")
                .termsOfServiceUrl("http://127.0.0.1:8080/")
                .contact(new Contact("PROJECT_AUTHOR",
                        "http://127.0.0.1:8080/PROJECT_NAME/doc.html",""))
                .version("v1.0.0").build();

        return apiInfo;
    }

}