package FILE_PACKAGE_NAME;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("MAPPER_PACKAGE")
@SpringBootApplication
public class APPLICATION_FILE_NAME{

    public static void main(String[] args) {
        SpringApplication.run(APPLICATION_FILE_NAME.class, args);
    }
}
