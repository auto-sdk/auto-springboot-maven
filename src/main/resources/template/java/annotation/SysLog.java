package FILE_PACKAGE_NAME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 日志注解接口
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLog {
	
	/**
	 * 操作类型
	 * @return
	 */
	public String operateType();
	
	/**
	 * 系统标识
	 * @return
	 */
	public String sysCode() default "";
}
