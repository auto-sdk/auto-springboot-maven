package FILE_PACKAGE_NAME;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import FILE_PACKAGE_NAME.SysLog;

/**
 * 日志切面类
 */
@Component
@Aspect
public class LogAspect {
	
	@Around(value = "@annotation(sysLog)") // aspect增强注解，对存在该注解方法的前后做拦截
	public Object logAroud(ProceedingJoinPoint joinPoint, SysLog sysLog)
			throws Throwable {
		
		// 执行方法体
		Object result = joinPoint.proceed();
		
		return result;
	}

	@AfterThrowing(value = "@annotation(sysLog)",throwing = "e")
	public void afterThrowing(JoinPoint joinPoint, SysLog sysLog,Throwable e)
			throws Throwable {
		e.printStackTrace();
	}
}
