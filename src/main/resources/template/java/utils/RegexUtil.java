package FILE_PACKAGE_NAME;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式校验
 * 
 */
public class RegexUtil {

	/**
	 * 是手机号吗？
	 *
	 * @param regStr
	 * @return
	 */
	public static boolean isTelephone(String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "^[1]\\d{10}";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

	/**
	 * 是邮箱吗？
	 *
	 * @param regStr
	 * @return
	 */
	public static boolean isEmail(String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "\\w+@\\w+\\.\\w+";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

	/**
	 * 是身份证号？
	 *
	 * @param regStr
	 * @return
	 */
	public static boolean isIdentityId(String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "^[1-9](\\d{13}|\\d{16})([a-z]|[A-Z]|[0-9])";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

	/**
	 * 是网址吗？
	 *
	 * @param regStr
	 * @return
	 */
	public static boolean isWebUrl(String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

	/**
	 * 是以某字符串开头，后面接数字的字符串吗？
	 *
	 * @param startStr
	 * @param regStr
	 * @return
	 */
	public static boolean isStartStr(String startStr, String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "^" + startStr + "\\d+";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

	/**
	 * 是数字或者数字逗号拼接的字符串吗？
	 *
	 * @param regStr 例如 1  或 1,2,3
	 * @return
	 */
	public static boolean isNumAndQuot(String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "^\\d+(,\\d+)*$";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

	/**
	 * 是任意字符或者字符按逗号拼接的字符串吗？
	 *
	 * @param regStr 例如 '1'或'31','2v','3a'
	 * @return
	 */
	public static boolean isCharAndQuot(String regStr) {
		if (regStr != null && !"".equals(regStr)) {
			String reg = "^'[\\w\\W]+'(,'[\\w\\W]+')*$";
			Pattern p = Pattern.compile(reg);
			Matcher m = p.matcher(regStr);
			return m.matches();
		}
		return false;
	}

}