package FILE_PACKAGE_NAME;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice(basePackages = "CONTROLLER_PACKAGE")
public class MyControllerAdvice {

    /**
     * 全局异常处理，反正异常返回统一格式的map
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ResultEntity exceptionHandler(Exception ex){

        return new ResultEntity<Object>(ConstantUtil.CODE_500,ex.getMessage());
     }
}