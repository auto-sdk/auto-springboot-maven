package FILE_PACKAGE_NAME;

/**
 * 字符串操作类
 */
public class StringUtil {

	/**
	 * 首字母转小写
	 * @param str
	 * @return
	 */
	public static String toLowerCaseFirstOne(String str) {
		if (Character.isLowerCase(str.charAt(0))) {
			return str;
		} else {
			return (new StringBuilder())
					.append(Character.toLowerCase(str.charAt(0)))
					.append(str.substring(1)).toString();
		}
	}

	/**
	 * 首字母转大写
	 * @param str
	 * @return
	 */
	public static String toUpperCaseFirstOne(String str) {
		if (Character.isUpperCase(str.charAt(0))) {
			return str;
		} else {
			return (new StringBuilder())
					.append(Character.toUpperCase(str.charAt(0)))
					.append(str.substring(1)).toString();
		}
	}

	/**
	 * 字符串判空
	 * @param str
	 * @return
	 */
	public static boolean isNull(String str) {
		if(str==null || "".equals(str)){
			return true;
		}
		return false;
	}

	/**
	 * 整数判空
	 * @param in
	 * @return
	 */
	public static boolean isNull(Integer in) {
		if(in==null){
			return true;
		}
		return false;
	}

	/**
	 * 长整型判空
	 * @param lon
	 * @return
	 */
	public static boolean isNull(Long lon) {
		if(lon==null){
			return true;
		}
		return false;
	}

	/**
	 * 双精度判空
	 * @param d
	 * @return
	 */
	public static boolean isNull(Double d) {
		if(d==null){
			return true;
		}
		return false;
	}

	/**
	 * 单精度判空
	 * @param f
	 * @return
	 */
	public static boolean isNull(Float f) {
		if(f==null){
			return true;
		}
		return false;
	}

	/**
	 * 字节数组判空
	 * @param bt
	 * @return
	 */
	public static boolean isNull(byte[] bt) {
		if(bt==null){
			return true;
		}
		return false;
	}

	/**
	 * 字符串判非空
	 * @param str
	 * @return
	 */
	public static boolean isNotNull(String str) {
		if(str!=null && !"".equals(str)){
			return true;
		}
		return false;
	}
	
	/**
	 * 对象判非空
	 * @param obj
	 * @return
	 */
	public static boolean isNotNull(Object obj) {
		if(obj!=null && !"".equals(obj.toString())){
			return true;
		}
		return false;
	}
	
	/**
	 * 对象判空
	 * @param obj
	 * @return
	 */
	public static boolean isNull(Object obj) {
		if(obj==null || "".equals(obj.toString())){
			return true;
		}
		return false;
	}

	/**
	 * 整数判非空
	 * @param in
	 * @return
	 */
	public static boolean isNotNull(Integer in) {
		if(in!=null){
			return true;
		}
		return false;
	}

	/**
	 * 长整数判非空
	 * @param lon
	 * @return
	 */
	public static boolean isNotNull(Long lon) {
		if(lon!=null){
			return true;
		}
		return false;
	}

	/**
	 * 双精度非空
	 * @param d
	 * @return
	 */
	public static boolean isNotNull(Double d) {
		if(d!=null){
			return true;
		}
		return false;
	}

	/**
	 * 单精度非空
	 * @param f
	 * @return
	 */
	public static boolean isNotNull(Float f) {
		if(f!=null){
			return true;
		}
		return false;
	}

	/**
	 * 字节数组判空
	 * @param bt
	 * @return
	 */
	public static boolean isNotNull(byte[] bt) {
		if(bt!=null){
			return true;
		}
		return false;
	}

	/**
	 * 将路径转成包
	 * @param path
	 * @return
	 */
	public static String parsePackName(String path){
		StringBuffer sb=new StringBuffer();
		String [] arr=null;
		if(path.indexOf("/")!=-1){
			arr=path.split("/");
		}
		if(path.indexOf("\\")!=-1){
			arr=path.split("\\\\");
		}
		sb.append(arr[1]);
		for(int i=2;i<arr.length;i++){
		sb.append("."+arr[i]);
		}
		return sb.toString();
	}

	/**
	 * 字符串转换驼峰命名
	 * @param str
	 * @return
	 */
	public static String toUnderscoreToCamelCase(String str){
		String camelStr = "";
		if(isNotNull(str)){
			String [] items = str.split("_");
			for(int i=0;i<items.length;i++){
				if(i==0){
					camelStr += toLowerCaseFirstOne(items[i].toLowerCase());
				}else{
					camelStr += toUpperCaseFirstOne(items[i].toLowerCase());
				}
			}
		}
		return camelStr;
	}
	
	/**
	 * 字符串转化 添加单引号  例如 a,b,c 转化成 'a','b','c'
	 * @param str
	 * @return
	 */
	public static String str2CharAndQuot(String str){
		String newStr = "";
		if(isNotNull(str)){
			String [] array = str.split(",");
			for(int i=0;i<array.length;i++){
				if(i==0){
					newStr+="'"+array[i]+"'";
				}else{
					newStr+=",'"+array[i]+"'";
				}
			}
		}
		return newStr;
	}

	/**
	 * 数组转单引号字符串
	 * @param array
	 * @return
	 */
	public static String array2CharStr(String ... array){
		String newStr = "";
		if(array!=null && array.length>0){
			for(int i=0;i<array.length;i++){
				if(i==0){
					newStr+="'"+array[i]+"'";
				}else{
					newStr+=",'"+array[i]+"'";
				}
			}
		}
		return newStr;
	}
}