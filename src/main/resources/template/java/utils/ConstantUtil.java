package FILE_PACKAGE_NAME;

/**
 * 定义常量类
 *
 */
public class ConstantUtil {

	/**
	 * cookie存入token时的键
	 */
	public final static String SESSION_TOKEN_NAME="user_session_token";

	/**
	 * 请求中header中的token参数
	 */
	public final static String HEADER_PARAM_TOKEN="authToken";

	/**
	 * 创建accessKey的custName
	 */
	public final static String ACCESS_KEY_CUST_NAME="authToken.custName";

	/**
	 * 创建accessKey的custPass
	 */
	public final static String ACCESS_KEY_CUST_PASS="authToken.custPass";

	/**
	 * accessKey年限时效
	 */
	public final static int ACCESS_KEY_VALID_YEAR=100;

	/**
	 * 系统管理员 编码
	 */
	public final static String ROLE_CODE_ADMINISTRATOR="SUPERADMIN";

	/**
	 * 数据有效状态 Y
	 */
	public final static String IS_VALID_Y = "Y";

	/**
	 * 数据有效状态 N
	 */
	public final static String IS_VALID_N = "N";


	/**
	 * 客户端类型PC
	 */
	public final static String CUST_TYPE_PC = "PC";

	/**
	 * 客户端类型ANDROID
	 */
	public final static String CUST_TYPE_ANDROID = "ANDROID";

	/**
	 * 客户端类型IOS
	 */
	public final static String CUST_TYPE_IOS= "IOS";

	/**
	 * 系统标识 sysCode
	 */
	public final static String DEAULT_SYS_CODE= "PROJECT_NAME";

	/**
	 * redis存入accessKey的键
	 */
	public final static String REDIS_KEY_ACCESS_KEY=DEAULT_SYS_CODE+"_accessKey";

	/**
	 * redis存入的key 字典map
	 */
	public static final String REDIS_KEY_DICT_MAP=DEAULT_SYS_CODE+"_dict_list_map";

	/**
	 * redis存入的key 字典树形集合
	 */
	public static final String REDIS_KEY_DICT_TREE_LIST=DEAULT_SYS_CODE+"_dict_tree_list";

	/**
	 * 是否清除过期token
	 */
	public static final String CLEAR_TOKEN_FLAG="clearTokenFlag";

	/**
	 * 模块根节点ID
	 */
	public static final String DEFAULT_MODULE_ROOT_ID="0";

	/**
	 * 前端表格列表选中属性  LAY_CHECKED
	 */
	public static final String LAY_CHECKED_KEY="LAY_CHECKED";

	/**
	 * 是否查询所有的标识
	 */
	public static final String SELECT_ALL_TYPE="all";


	/**
	 * 日志类型 登录日志
	 */
	public static final String LOG_TYPE_LOGIN="login_log";


	/**
	 * 日志类型 操作日志
	 */
	public static final String LOG_TYPE_OP="op_log";


	/**
	 * 错误代号值
	 */
	public final static String CODE_200="200"; //操作成功
	public final static String CODE_401="401"; //验证码错误
	public final static String CODE_402="402"; //用户名或密码错误
	public final static String CODE_403="403"; //没有操作权限
	public final static String CODE_404="404"; //参数错误
	public final static String CODE_406="406"; //账号在另一地点登录
	public final static String CODE_407="407"; //该值已经存在
	public final static String CODE_408="408"; //用户未登录
	public final static String CODE_500="500"; //系统异常

	/**
	 * 错误消息
	 */
	public final static String MSG_ADD_SUCCESS="新增成功";
	public final static String MSG_EDIT_SUCCESS="修改成功";
	public final static String MSG_DEL_SUCCESS="删除成功";
	public final static String MSG_ADD_ERROR="新增失败";
	public final static String MSG_EDIT_ERROR="修改失败";
	public final static String MSG_DEL_ERROR="删除失败";
	public final static String MSG_SELECT_ERROR="查询失败";
	public final static String MSG_PAGE_DATA_ERROR="分页参数错误";

	public final static String MSG_401="验证码错误";
	public final static String MSG_402="用户名或密码错误";
	public final static String MSG_403="没有操作权限";
	public final static String MSG_404="参数错误";
	public final static String MSG_406="您的账号在另一地点登录";
	public final static String MSG_500="系统异常";

	/**
	 * 操作成功信息
	 */
	public final static String OP_MSG_SUCCESS="操作成功";

	/**
	 * 操作失败信息
	 */
	public final static String OP_MSG_ERROR="操作失败";

}
