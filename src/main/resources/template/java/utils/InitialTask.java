package FILE_PACKAGE_NAME;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 初始化启动执行
 */
@Component
public class InitialTask {

    @PostConstruct
    public void init(){

    }
}
