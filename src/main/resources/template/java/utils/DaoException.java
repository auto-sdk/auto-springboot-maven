package FILE_PACKAGE_NAME;

/**
 * Daoy异常类
 * 
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	private String message;
	private Exception exception;

	public DaoException() {
		if (message != null && !"".equals(message)) {
			System.out.println(message);
		}
	}

	public DaoException(Exception exception, String message) {
		this.message = message;
		this.exception = exception;
	}

	public DaoException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
	@Override
	public void printStackTrace() {
		System.out.println(message);
		if (exception != null) {
			exception.printStackTrace();
		} else {
			super.printStackTrace();
		}
	}

}