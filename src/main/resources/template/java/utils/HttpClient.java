package FILE_PACKAGE_NAME;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HttpClient {

    /**
     * http请求
     * @param requestUrl
     * @param requestMethod
     * @param jsonData
     * @return
     */
    public static JSONObject httpRequest(String requestUrl,String requestMethod,String jsonData){
        JSONObject json=null;
        try {
            URL urlGet=new URL(requestUrl);
            HttpURLConnection http=(HttpURLConnection) urlGet.openConnection();

            http.setDoOutput(true);
            http.setDoInput(true);
            http.setUseCaches(false);
            http.setRequestMethod(requestMethod);

            if(jsonData!=null && !"".equals(jsonData)){
                String message = setJsonData(jsonData);
                OutputStream outputStream=http.getOutputStream();
                outputStream.write(message.getBytes("UTF-8"));
                outputStream.close();
            }

            InputStream is=http.getInputStream();
            InputStreamReader isr=new InputStreamReader(is,"UTF-8");
            BufferedReader reader=new BufferedReader(isr);
            String str=null;
            StringBuffer sb=new StringBuffer();

            while((str=reader.readLine())!=null){
                sb.append(str);
            }

            //释放资源
            isr.close();
            reader.close();
            is.close();
            is=null;
            http.disconnect();
            if(sb.toString()!=null && sb.toString().indexOf("{")!=-1 &&  sb.toString().indexOf("}")!=-1){
                json=JSONObject.parseObject(sb.toString());
            }else if(sb.toString()!=null && !"".equals(sb.toString())){
                String data=sb.toString().replace("'", "\"");
                String text="{ 'data':'"+data+"'}";
                json=JSONObject.parseObject(text);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }


    /**
     * http请求
     * @param requestUrl
     * @param requestMethod
     * @param map
     * @return
     */
    public static JSONObject httpRequest(String requestUrl,String requestMethod,Map<String,Object> map){
        JSONObject json=null;
        try {
            URL urlGet=new URL(requestUrl);
            HttpURLConnection http=(HttpURLConnection) urlGet.openConnection();

            http.setDoOutput(true);
            http.setDoInput(true);
            http.setUseCaches(false);
            http.setRequestMethod(requestMethod);

            if(map!=null){
                String message = setMap(map);
                OutputStream outputStream=http.getOutputStream();
                outputStream.write(message.getBytes("UTF-8"));
                outputStream.close();
            }

            InputStream is=http.getInputStream();
            InputStreamReader isr=new InputStreamReader(is,"UTF-8");
            BufferedReader reader=new BufferedReader(isr);
            String str=null;
            StringBuffer sb=new StringBuffer();

            while((str=reader.readLine())!=null){
                sb.append(str);
            }

            //释放资源
            isr.close();
            reader.close();
            is.close();
            is=null;
            http.disconnect();
            if(sb.toString()!=null && sb.toString().indexOf("{")!=-1 &&  sb.toString().indexOf("}")!=-1){
                json=JSONObject.parseObject(sb.toString());
            }else if(sb.toString()!=null){
                String data=sb.toString().replace("'", "\"");
                String text="{ 'data':'"+data+"'}";
                json=JSONObject.parseObject(text);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * https请求
     * @param requestUrl
     * @param requestMethod
     * @param map
     * @return
     */
	public static JSONObject httpsRequest(String requestUrl, String requestMethod, Map<String,Object> map){
		JSONObject json=null;
		try {
			TrustManager[] tm={new MyX509TrustManager()};
			SSLContext ssl=SSLContext.getInstance("SSL", "SunJSSE");
			ssl.init(null, tm, new SecureRandom());
			
			SSLSocketFactory ssf=ssl.getSocketFactory();
			
			URL urlGet=new URL(requestUrl);
			
			HttpsURLConnection http=(HttpsURLConnection) urlGet.openConnection(); 
			http.setHostnameVerifier(new HttpClient().new TrustAnyHostnameVerifier());
			http.setSSLSocketFactory(ssf);
			http.setDoOutput(true);
			http.setDoInput(true);
			http.setUseCaches(false);
			http.setRequestMethod(requestMethod);
			
			System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
			System.setProperty("sun.net.client.defaultReadTimeout", "30000");

			if(map!=null){
                String message = setMap(map);
				OutputStream outputStream=http.getOutputStream();
				outputStream.write(message.getBytes("UTF-8"));
				outputStream.close();
			}
			
			InputStream is=http.getInputStream();
			InputStreamReader isr=new InputStreamReader(is,"UTF-8");
			BufferedReader reader=new BufferedReader(isr);
			String str=null;
			StringBuffer sb=new StringBuffer();
			
			while((str=reader.readLine())!=null){
				sb.append(str);
			}
			
			//释放资源
			isr.close();
			reader.close();
			is.close();
			is=null;
			http.disconnect();
			if(sb.toString()!=null && sb.toString().indexOf("{")!=-1 &&  sb.toString().indexOf("}")!=-1){
				json=JSONObject.parseObject(sb.toString());
			}else if("[]".equals(sb.toString())){
				json=new JSONObject();
			}else{
				String data=sb.toString().replace("'", "\"");
				String text="{'data':'"+data+"'}";
				json=JSONObject.parseObject(text);
			}
			json.put("error_code", 200);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return json;
	}

    /**
     * https请求
     * @param requestUrl
     * @param requestMethod
     * @param jsonData
     * @return
     */
    public static JSONObject httpsRequest(String requestUrl, String requestMethod, String jsonData){
        JSONObject json=null;
        try {
            TrustManager[] tm={new MyX509TrustManager()};
            SSLContext ssl=SSLContext.getInstance("SSL", "SunJSSE");
            ssl.init(null, tm, new SecureRandom());

            SSLSocketFactory ssf=ssl.getSocketFactory();

            URL urlGet=new URL(requestUrl);

            HttpsURLConnection http=(HttpsURLConnection) urlGet.openConnection();
            http.setHostnameVerifier(new HttpClient().new TrustAnyHostnameVerifier());
            http.setSSLSocketFactory(ssf);
            http.setDoOutput(true);
            http.setDoInput(true);
            http.setUseCaches(false);
            http.setRequestMethod(requestMethod);

            System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
            System.setProperty("sun.net.client.defaultReadTimeout", "30000");

            if(jsonData!=null && !"".equals(jsonData)){
                String message = setJsonData(jsonData);
                OutputStream outputStream=http.getOutputStream();
                outputStream.write(message.getBytes("UTF-8"));
                outputStream.close();
            }

            InputStream is=http.getInputStream();
            InputStreamReader isr=new InputStreamReader(is,"UTF-8");
            BufferedReader reader=new BufferedReader(isr);
            String str=null;
            StringBuffer sb=new StringBuffer();

            while((str=reader.readLine())!=null){
                sb.append(str);
            }

            //释放资源
            isr.close();
            reader.close();
            is.close();
            is=null;
            http.disconnect();
            if(sb.toString()!=null && sb.toString().indexOf("{")!=-1 &&  sb.toString().indexOf("}")!=-1){
                json=JSONObject.parseObject(sb.toString());
            }else if("[]".equals(sb.toString())){
                json=new JSONObject();
            }else{
                String data=sb.toString().replace("'", "\"");
                String text="{'data':'"+data+"'}";
                json=JSONObject.parseObject(text);
            }
            json.put("error_code", 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

	public class TrustAnyHostnameVerifier implements HostnameVerifier {

		public boolean verify(String arg0, SSLSession arg1) {
			return true;
		}
    }

	public static String setMap(Map<String, Object> map) {
		String message = "";
		if (map != null) {
			for (Entry<String, Object> entry : map.entrySet()) {
				message += entry.getKey() + "=" + entry.getValue() + "&";
			}
		}
		return message;
	}

	public static String setJsonData(String jsonData) {
        String message = "";
        if(jsonData!=null && !"".equals(jsonData)){
            JSONObject jsonObject = JSON.parseObject(jsonData);
            if (jsonObject != null) {
                for (Entry<String, Object> entry : jsonObject.entrySet()) {
                    message += entry.getKey() + "=" + entry.getValue() + "&";
                }
            }
        }
		return message;
	}

    public static void main(String[] args) {
        String url = "";
        Map<String, Object> map = new HashMap<String,Object>();
        JSONObject jsonObject=HttpClient.httpsRequest(url, "POST", map);
        System.out.println(jsonObject.toString());
    }
}

class MyX509TrustManager implements X509TrustManager{

	public void checkClientTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		
	}

	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
		
	}

	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}
}
