package FILE_PACKAGE_NAME;

/**
 * 校验异常类
 */
public class ValidateException extends Exception {

	private static final long serialVersionUID = 1L;

	private String message;
	private Exception exception;

	public ValidateException() {
		if (message != null && !"".equals(message)) {
			System.out.println(message);
		}
	}

	public ValidateException(Exception exception, String message) {
		this.message = message;
		this.exception = exception;
	}

	public ValidateException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
	@Override
	public void printStackTrace() {
		System.out.println(message);
		if (exception != null) {
			exception.printStackTrace();
		} else {
			super.printStackTrace();
		}
	}

}