package FILE_PACKAGE_NAME;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * JSON数据模板
 */
@ApiModel(value = "请求结果",description="请求结果")
public class ResultEntity<T> {

    @ApiModelProperty("对象")
    private T result;

    @ApiModelProperty("消息")
    private String msg;

    @ApiModelProperty(value="总记录数",example = "1")
    private Integer total;

    @ApiModelProperty("错误代码，200成功，其他失败")
    private String code;

    @ApiModelProperty(hidden = true)
    private int flag = 0;

    public ResultEntity(String code, String msg) {
        this.msg = msg;
        this.code = code;
        flag = 1;
    }

    public ResultEntity(String code, T result) {
        this.result = result;
        this.code = code;
        flag = 2;
    }

    public ResultEntity(String code, T result, Integer total) {
        this.result = result;
        this.code = code;
        this.total = total;
        flag = 3;
    }

    public ResultEntity(String code, T result, String msg) {
        this.result = result;
        this.code = code;
        this.msg = msg;
        flag = 4;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        JSONObject jsonObject = (JSONObject) JSON.toJSON(this);
        if (flag == 1) {
            jsonObject.remove("result");
            jsonObject.remove("total");
        } else if (flag == 2) {
            jsonObject.remove("total");
            jsonObject.remove("msg");
        } else if (flag == 3) {
            jsonObject.remove("msg");
        } else if (flag == 4) {
            jsonObject.remove("total");
        }
        return jsonObject.toString();
    }
}