package FILE_PACKAGE_NAME;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 读取资源配置文件
 */
public class ConfigProperties extends Properties{

    private static final long serialVersionUID = 1L;
    private static ConfigProperties instance = null;


    /**
     * 外部调用这个方法来获取唯一的一个实例
     */
    public synchronized static ConfigProperties getInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new ConfigProperties();
            return instance;
        }
    }

    private ConfigProperties(){
        try {
            InputStream is = ConfigProperties.class.getResourceAsStream("/CONFIG_FILE_PATH");
            this.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 获取参数值
     * @param key
     * @return
     */
    public static String getValue(String key) {
    	String value = ConfigProperties.getInstance().getProperty(key);
    	return value;
    }
}
