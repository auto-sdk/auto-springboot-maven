package com.lp.auto.sdk.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lp.auto.sdk.utils.*;
import org.apache.commons.io.FileUtils;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateController;
import com.lp.auto.sdk.service.CreateEntity;
import com.lp.auto.sdk.service.CreateMapper;
import com.lp.auto.sdk.service.CreateMapperXML;
import com.lp.auto.sdk.service.CreateService;
import com.lp.auto.sdk.service.CreateServiceImpl;
import com.lp.auto.sdk.service.impl.CreateControllerImpl;
import com.lp.auto.sdk.service.impl.CreateEntityImpl;
import com.lp.auto.sdk.service.impl.CreateMapperFileImpl;
import com.lp.auto.sdk.service.impl.CreateMapperXmlImpl;
import com.lp.auto.sdk.service.impl.CreateServiceFileImpl;
import com.lp.auto.sdk.service.impl.CreateServiceImplFileImpl;

/**
 * 自动生成项目文件
 *
 * @author 兰平
 * @version 1.5
 * @date 2017-7-16
 */
public class DB2ProjectManager extends ProjectManager {

    /**
     * 创建web项目相关文件
     *
     * @throws Exception
     */
    public void createFiles(Connector connector, ProjInfo projInfo) throws Exception {
        BaseDao baseDao = new BaseDao(connector);
        Connection con = baseDao.getConnection();

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            if (con == null) {
                System.out.println("数据库连接失败");
                return;
            }
            String sql = "select * from sysibm.systables where type='T' and creator='" + connector.getSchema() + "'";
            pstmt = con.prepareStatement(sql); // 预编译对象
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String tableName = rs.getString("name");
                tableName = tableName.toLowerCase();
                String tableComment = rs.getString("remarks");
                tableComment = tableComment == null ? "" : tableComment;
                System.out.println("数据库表[" + tableName + "]");
                sql = "select * from syscat.columns where tabschema = '" + connector.getSchema() + "' and tabname = upper('" + tableName + "') order by colno ";
                pstmt = con.prepareStatement(sql); // 预编译对象
                ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集

                EntityInfo entityInfo = new EntityInfo();
                entityInfo.setTableName(tableName);
                entityInfo.setTableComment(tableComment);

                CreateEntity createEntity = CreateEntityImpl.getCreateEntity(connector.getDbType());
                entityInfo = createEntity.writeFile(result, entityInfo, projInfo);

                CreateMapperXML createXML = new CreateMapperXmlImpl();
                createXML.writeFile(entityInfo, projInfo, connector);

                CreateMapper createMapper = new CreateMapperFileImpl();
                createMapper.writeFile(entityInfo, projInfo);

                CreateService createService = new CreateServiceFileImpl();
                createService.writeFile(entityInfo, projInfo);

                CreateServiceImpl createServiceImpl = new CreateServiceImplFileImpl();
                createServiceImpl.writeFile(entityInfo, projInfo, connector.getDbType());

                CreateController createController = new CreateControllerImpl();
                createController.writeFile(entityInfo, projInfo);

            }

            FileUtil.copyPublicFiles(projInfo,connector);

            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
            //如果是生成zip
            if("true".equals(createZipFlag)){
            	//压缩文件
                String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
                String outZipPath = rootDir + projInfo.getProjectName() + Constant.FILE_SUFFIX_ZIP;
                FileOutputStream fos1 = new FileOutputStream(new File(outZipPath));
                ZipUtils.toZip(rootDir + projInfo.getProjectName(), fos1, true);

                //删除文件夹
                File directory = new File(rootDir + projInfo.getProjectName());
                FileUtils.deleteDirectory(directory);
                System.out.println("执行成功，"+outZipPath+"已生成，请查看！");
            }else{
            	System.out.println("执行成功，请刷新项目！");
            }
            

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
        	baseDao.closeAll(con, pstmt, rs);
        }
    }

    /**
     * 单张表对应的项目文件
     *
     * @param connector
     * @param projInfo
     * @param entityInfo
     * @throws Exception
     */
    public void createFilesByOneTable(Connector connector, ProjInfo projInfo,
                                      EntityInfo entityInfo) throws Exception {
        BaseDao baseDao = new BaseDao(connector);
        Connection con = baseDao.getConnection();
        String tableName = entityInfo.getTableName().toLowerCase();

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            if (con == null) {
                System.out.println("数据库连接失败");
                return;
            }
            String sql = "select * from sysibm.systables where type='T' and creator='" + connector.getSchema() + "'";
            pstmt = con.prepareStatement(sql); // 预编译对象
            rs = pstmt.executeQuery();
            boolean flag = false;
            while (rs.next()) {
                if (tableName.equals(rs.getString("name"))) {
                    flag = true;
                    //String comment = rs.getString("remarks");
                    //String tableComment = entityInfo.getTableComment() == null ? comment : entityInfo.getTableComment();
                    System.out.println("数据库表[" + tableName + "]");
                    sql = "select * from syscat.columns where tabschema = '" + connector.getSchema() + "' and tabname = upper('" + tableName + "') order by colno ";
                    pstmt = con.prepareStatement(sql); // 预编译对象
                    ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集

                    CreateEntity createEntity = CreateEntityImpl.getCreateEntity(connector.getDbType());
                    entityInfo = createEntity.writeFile(result, entityInfo, projInfo);

                    CreateMapperXML createXML = new CreateMapperXmlImpl();
                    createXML.writeFile(entityInfo, projInfo, connector);

                    CreateMapper createMapper = new CreateMapperFileImpl();
                    createMapper.writeFile(entityInfo, projInfo);

                    CreateService createService = new CreateServiceFileImpl();
                    createService.writeFile(entityInfo, projInfo);

                    CreateServiceImpl createServiceImpl = new CreateServiceImplFileImpl();
                    createServiceImpl.writeFile(entityInfo, projInfo, connector.getDbType());

                    CreateController createController = new CreateControllerImpl();
                    createController.writeFile(entityInfo, projInfo);

                }
            }
            if (flag == false) {
                throw new Exception("表" + tableName + "不存在");
            }

            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
            //如果是生成zip
            if("true".equals(createZipFlag)){
	            //压缩文件
	            String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
	            String outZipPath = rootDir + projInfo.getProjectName() + Constant.FILE_SUFFIX_ZIP;
	            FileOutputStream fos1 = new FileOutputStream(new File(outZipPath));
	            ZipUtils.toZip(rootDir + projInfo.getProjectName(), fos1, true);
	
	            //删除文件夹
	            File directory = new File(rootDir + projInfo.getProjectName());
	            FileUtils.deleteDirectory(directory);
	
	            System.out.println("执行成功，"+outZipPath+"已生成，请查看！");
            }else{
            	System.out.println("执行成功，请刷新项目！");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            baseDao.closeAll(con, pstmt, rs);
        }
    }
}
