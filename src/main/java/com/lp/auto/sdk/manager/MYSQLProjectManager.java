package com.lp.auto.sdk.manager;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.*;
import com.lp.auto.sdk.service.impl.*;
import com.lp.auto.sdk.utils.*;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.*;

/**
 * 自动生成项目文件
 *
 * @author 兰平
 * @version 1.5
 * @date 2017-7-16
 */
public class MYSQLProjectManager extends ProjectManager {

    /**
     * 创建web项目相关文件
     *
     * @throws Exception
     */
    public void createFiles(Connector connector, ProjInfo projInfo) throws Exception {
        BaseDao baseDao = new BaseDao(connector);
        Connection con = baseDao.getConnection();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            if (con == null) {
                System.out.println("数据库连接失败");
                return;
            }
            String schema = con.getCatalog();
            System.out.println("数据库名：" + schema);
            DatabaseMetaData dmd = con.getMetaData();
            rs = dmd.getTables(null, schema, "%", null);
            while (rs.next()) {
                String tableName = rs.getString("TABLE_NAME");
                System.out.println("数据库表[" + tableName + "]");
                //查询表注释
                String tableComment = getTableComment(con, schema, tableName);
                String sql = "select * from information_schema.columns where " +
                        " table_schema = '" + schema + "' and table_name = '" + tableName + "' ORDER BY ORDINAL_POSITION ";
                pstmt = con.prepareStatement(sql); // 预编译对象
                ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集

                EntityInfo entityInfo = new EntityInfo();
                entityInfo.setTableName(tableName);
                entityInfo.setTableComment(tableComment);

                CreateEntity createEntity = CreateEntityImpl.getCreateEntity(connector.getDbType());
                entityInfo = createEntity.writeFile(result, entityInfo, projInfo);

                CreateMapperXML createXML = new CreateMapperXmlImpl();
                createXML.writeFile(entityInfo, projInfo, connector);

                CreateMapper createMapper = new CreateMapperFileImpl();
                createMapper.writeFile(entityInfo, projInfo);

                CreateService createService = new CreateServiceFileImpl();
                createService.writeFile(entityInfo, projInfo);

                CreateServiceImpl createServiceImpl = new CreateServiceImplFileImpl();
                createServiceImpl.writeFile(entityInfo, projInfo, connector.getDbType());

                CreateController createController = new CreateControllerImpl();
                createController.writeFile(entityInfo, projInfo);

            }

            FileUtil.copyPublicFiles(projInfo,connector);

            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
            //如果是生成zip
            if("true".equals(createZipFlag)){
            	//压缩文件
            	String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
            	String outZipPath = rootDir + projInfo.getProjectName() + Constant.FILE_SUFFIX_ZIP;
            	FileOutputStream fos1 = new FileOutputStream(new File(outZipPath));
            	ZipUtils.toZip(rootDir + projInfo.getProjectName(), fos1, true);
            	
            	//删除文件夹
            	File directory = new File(rootDir + projInfo.getProjectName());
            	FileUtils.deleteDirectory(directory);
            	
            	System.out.println("执行成功，"+outZipPath+"已生成，请查看！");
            }else{
            	System.out.println("执行成功，请刷新项目！");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
        	baseDao.closeAll(con, pstmt, rs);
        }
    }

    /**
     * 单张表对应的项目文件
     */
    public void createFilesByOneTable(Connector connector, ProjInfo projInfo,
                                      EntityInfo entityInfo) throws Exception {
        BaseDao baseDao = new BaseDao(connector);
        Connection con = baseDao.getConnection();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            if (con == null) {
                System.out.println("数据库连接失败");
                return;
            }
            String schema = con.getCatalog();
            System.out.println("数据库表[" + entityInfo.getTableName() + "]");
            String sql = "select * from information_schema.columns where " +
                    " table_schema = '" + schema + "' and table_name = '" + entityInfo.getTableName() + "' ORDER BY ORDINAL_POSITION ";
            pstmt = con.prepareStatement(sql); // 预编译对象
            ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集

            entityInfo.setTableName(entityInfo.getTableName());
            entityInfo.setTableComment(entityInfo.getTableComment());

            CreateEntity createEntity = CreateEntityImpl.getCreateEntity(connector.getDbType());
            entityInfo = createEntity.writeFile(result, entityInfo, projInfo);

            CreateMapperXML createXML = new CreateMapperXmlImpl();
            createXML.writeFile(entityInfo, projInfo, connector);

            CreateMapper createMapper = new CreateMapperFileImpl();
            createMapper.writeFile(entityInfo, projInfo);

            CreateService createService = new CreateServiceFileImpl();
            createService.writeFile(entityInfo, projInfo);

            CreateServiceImpl createServiceImpl = new CreateServiceImplFileImpl();
            createServiceImpl.writeFile(entityInfo, projInfo, connector.getDbType());

            CreateController createController = new CreateControllerImpl();
            createController.writeFile(entityInfo, projInfo);

            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
            //如果是生成zip
            if("true".equals(createZipFlag)){
            	//压缩文件
                String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
                String outZipPath = rootDir + projInfo.getProjectName() + Constant.FILE_SUFFIX_ZIP;
                FileOutputStream fos1 = new FileOutputStream(new File(outZipPath));
                ZipUtils.toZip(rootDir + projInfo.getProjectName(), fos1, true);

                //删除文件夹
                File directory = new File(rootDir + projInfo.getProjectName());
                FileUtils.deleteDirectory(directory);
                System.out.println("执行成功，"+outZipPath+"已生成，请查看！");
            }else{
            	System.out.println("执行成功，请刷新项目！");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
        	baseDao.closeAll(con, pstmt, rs);
        }
    }


    private String getTableComment(Connection connection, String schema, String tableName) throws Exception {
        PreparedStatement pstmt = null;
        String sql = "select table_name,table_comment from information_schema.tables " +
                " where table_schema = '" + schema + "' and table_name ='" + tableName + "'";
        pstmt = connection.prepareStatement(sql); // 预编译对象
        ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集
        String comment = "";
        if (result != null && result.next()) {
            comment = result.getString("table_comment");
            if (comment != null && !"".equals(comment)) {
                if (comment.substring(comment.length() - 1).equals("表")) {
                    comment = comment.substring(0, comment.length() - 1);
                }
            } else {
                comment = "";
            }
        }
        return comment;
    }
}
