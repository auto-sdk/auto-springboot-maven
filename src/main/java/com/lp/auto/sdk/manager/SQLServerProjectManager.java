package com.lp.auto.sdk.manager;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.*;
import com.lp.auto.sdk.service.impl.*;
import com.lp.auto.sdk.utils.*;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 自动生成项目文件
 *
 * @author 兰平
 * @version 1.5
 * @date 2017-7-16
 */
public class SQLServerProjectManager extends ProjectManager {

    /**
     * 创建web项目相关文件
     *
     * @throws Exception
     */
    public void createFiles(Connector connector, ProjInfo projInfo) throws Exception {
        BaseDao baseDao = new BaseDao(connector);
        Connection con = baseDao.getConnection();

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        String sql = "select a.name, cast([value] as varchar(500))[value] from " +
                " sys.tables a left join sys.extended_properties g on " +
                " (a.object_id = g.major_id AND g.minor_id = 0)";
        try {
            if (con == null) {
                System.out.println("数据库连接失败");
                return;
            }
            pstmt = con.prepareStatement(sql); // 预编译对象
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String tableName = rs.getString("name");
                String tableComment = rs.getString("value");
                tableComment = tableComment == null ? "" : tableComment;
                System.out.println("数据库表[" + tableName + "]");
                sql = "SELECT a.column_id,a.is_identity, a.name as COLUMN_NAME ,typ.name as TYPE_NAME , " +
                        " cast([value] as varchar(500))[value] FROM sys.columns a " +
                        " left join sys.extended_properties g on (a.object_id = g.major_id AND g.minor_id = a.column_id) " +
                        " left join sys.types typ on (a.system_type_id = typ.system_type_id) " +
                        " WHERE object_id = (SELECT object_id FROM sys.tables WHERE name = '" + tableName + "') order by a.column_id ";
                pstmt = con.prepareStatement(sql); // 预编译对象
                ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集

                EntityInfo entityInfo = new EntityInfo();
                entityInfo.setTableName(tableName);
                entityInfo.setTableComment(tableComment);

                CreateEntity createEntity = CreateEntityImpl.getCreateEntity(connector.getDbType());
                entityInfo = createEntity.writeFile(result, entityInfo, projInfo);

                CreateMapperXML createXML = new CreateMapperXmlImpl();
                createXML.writeFile(entityInfo, projInfo, connector);

                CreateMapper createMapper = new CreateMapperFileImpl();
                createMapper.writeFile(entityInfo, projInfo);

                CreateService createService = new CreateServiceFileImpl();
                createService.writeFile(entityInfo, projInfo);

                CreateServiceImpl createServiceImpl = new CreateServiceImplFileImpl();
                createServiceImpl.writeFile(entityInfo, projInfo, connector.getDbType());

                CreateController createController = new CreateControllerImpl();
                createController.writeFile(entityInfo, projInfo);
            }

            FileUtil.copyPublicFiles(projInfo,connector);

            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
            //如果是生成zip
            if("true".equals(createZipFlag)){
            	//压缩文件
            	String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
            	String outZipPath = rootDir + projInfo.getProjectName() + Constant.FILE_SUFFIX_ZIP;
            	FileOutputStream fos1 = new FileOutputStream(new File(outZipPath));
            	ZipUtils.toZip(rootDir + projInfo.getProjectName(), fos1, true);
            	
            	//删除文件夹
            	File directory = new File(rootDir + projInfo.getProjectName());
            	FileUtils.deleteDirectory(directory);
            	
            	System.out.println("执行成功，"+outZipPath+"已生成，请查看！");
            }else{
            	System.out.println("执行成功，请刷新项目！");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            baseDao.closeAll(con, pstmt, rs);
        }
    }

    public void createFilesByOneTable(Connector connector, ProjInfo projInfo,
                                      EntityInfo entityInfo) throws Exception {
        BaseDao baseDao = new BaseDao(connector);
        Connection con = baseDao.getConnection();

        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            System.out.println("数据库表[" + entityInfo.getTableName() + "]");
            String sql = "SELECT a.column_id, a.name as COLUMN_NAME ,typ.name as TYPE_NAME , " +
                    " cast([value] as varchar(500))[value] FROM sys.columns a " +
                    " left join sys.extended_properties g on (a.object_id = g.major_id AND g.minor_id = a.column_id) " +
                    " left join sys.types typ on (a.system_type_id = typ.system_type_id) " +
                    " WHERE object_id = (SELECT object_id FROM sys.tables WHERE name = '" + entityInfo.getTableName() + "') order by a.column_id ";
            pstmt = con.prepareStatement(sql); // 预编译对象
            ResultSet result = pstmt.executeQuery(); // 执行查询语句，得到结果集

            entityInfo.setTableName(entityInfo.getTableName());
            entityInfo.setTableComment(entityInfo.getTableComment());

            CreateEntity createEntity = CreateEntityImpl.getCreateEntity(connector.getDbType());
            entityInfo = createEntity.writeFile(result, entityInfo, projInfo);

            CreateMapperXML createXML = new CreateMapperXmlImpl();
            createXML.writeFile(entityInfo, projInfo, connector);

            CreateMapper createMapper = new CreateMapperFileImpl();
            createMapper.writeFile(entityInfo, projInfo);

            CreateService createService = new CreateServiceFileImpl();
            createService.writeFile(entityInfo, projInfo);

            CreateServiceImpl createServiceImpl = new CreateServiceImplFileImpl();
            createServiceImpl.writeFile(entityInfo, projInfo, connector.getDbType());

            CreateController createController = new CreateControllerImpl();
            createController.writeFile(entityInfo, projInfo);

            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
            //如果是生成zip
            if("true".equals(createZipFlag)){
            	//压缩文件
                String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
                String outZipPath = rootDir + projInfo.getProjectName() + Constant.FILE_SUFFIX_ZIP;
                FileOutputStream fos1 = new FileOutputStream(new File(outZipPath));
                ZipUtils.toZip(rootDir + projInfo.getProjectName(), fos1, true);

                //删除文件夹
                File directory = new File(rootDir + projInfo.getProjectName());
                FileUtils.deleteDirectory(directory);

                System.out.println("执行成功，"+outZipPath+"已生成，请查看！");
            }else{
            	System.out.println("执行成功，请刷新项目！");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
        	baseDao.closeAll(con, pstmt, rs);
        }
    }

}
