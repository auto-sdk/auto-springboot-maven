package com.lp.auto.sdk.manager;

import com.lp.auto.sdk.utils.Constant;

/**
 * 创建项目
 *
 * @author lanping
 * @version 1.0
 * @date 2019-06-30
 **/
public class CreateProjectManager {

    public static ProjectManager getProjectManager(String dbType) {
        ProjectManager projectManager = null;
        if (Constant.DB_TYPE_DB2.equals(dbType)) {
            projectManager = new DB2ProjectManager();
        } else if (Constant.DB_TYPE_SQLSERVER.equals(dbType)) {
            projectManager = new SQLServerProjectManager();
        } else if (Constant.DB_TYPE_MYSQL.equals(dbType)) {
            projectManager = new MYSQLProjectManager();
        }
        return projectManager;
    }

}
