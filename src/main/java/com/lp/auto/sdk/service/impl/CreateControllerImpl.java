package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateController;
import com.lp.auto.sdk.utils.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateControllerImpl implements CreateController {

    /**
     * 创建Controller文件
     *
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     * @throws Exception
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception {

        String tableComment = entityInfo.getTableComment();
        String lowerPrimaryKey = MyStringUtil.toLowerCaseFirstOne(entityInfo.getPrimaryKey());
        String upperPrimaryKey = MyStringUtil.toUpperCaseFirstOne(entityInfo.getPrimaryKey());
        String primaryKeyType = entityInfo.getPrimaryKeyType();

        String upperEntityName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        String lowerEntityName = MyStringUtil.toLowerCaseFirstOne(entityInfo.getEntityName());

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_CONTROLLER_PATH);
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String utilPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String servicePackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);
        String accessBaseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        String annotaionPackage = basePackage+"."+PropertiesUtil.getConfigValue(Constant.KEY_ANNOTATION_PATH);

        String pageSelectCondition = getPageSelectCondition(entityInfo.getTableColnames(),
                entityInfo.getAttrComments(),entityInfo.getAttrTypes(),entityInfo.getTablePrimaryKey());

        String dirPath = PublicFileUtil.class.getClassLoader().getResource("").getPath();
        try {
            dirPath = URLDecoder.decode(dirPath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String srcFilePath = dirPath.replace("/target/test-classes/", "/target/classes/")+Constant.TEMPLATE_CONTROLLER_PATH;

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperEntityName + "Controller" + Constant.FILE_SUFFIX_JAVA;
        String destFilePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);

        Map<String,String> map = new HashMap<String,String>(){
            {
                put("FILE_PACKAGE_NAME",packageName);
                put("ENTITY_PACKAGE",entityPackName);
                put("UPPER_ENTITY_NAME",upperEntityName);
                put("TABLE_COMMENT",tableComment);
                put("PROJECT_AUTHOR",projInfo.getAuthor());
                put("NOW_DATE",DateUtil.getNowDate(DateUtil.DATE_PATTERN));
                put("LOWER_ENTITY_NAME",lowerEntityName);
                put("UPPER_PRIMARY_KEY",upperPrimaryKey);
                put("LOWER_PRIMARY_KEY",lowerPrimaryKey);
                put("UTILS_PACKAGE",utilPackage);
                put("SERVICE_PAKCAGE",servicePackage);
                put("ACCESS_BASE_URL",accessBaseUrl);
                put("ANNOTATION_PACKAGE",annotaionPackage);
                put("PAGE_SELECT_MORE_CONDITION",pageSelectCondition);
            }
        };
        FileUtil.copyFile(srcFilePath, destFilePath, map);
    }

    /**
     * 查询条件
     * @param tableColnames
     * @param attrComments
     * @param attrTypes
     * @param tablePrimaryKey
     * @return
     */
    public String getPageSelectCondition(List<String> tableColnames,List<String> attrComments,List<String> attrTypes,String tablePrimaryKey){
        StringBuffer sb = new StringBuffer();
        if(tableColnames!=null && tableColnames.size()>0
            && attrComments!=null && attrComments.size()>0 && attrTypes!=null && attrTypes.size()>0
            && attrTypes.size() == tableColnames.size() && attrComments.size()==tableColnames.size() ){
            for (int i=0;i<tableColnames.size();i++){
                for(int j=0;j<Constant.LIKE_NAME_ARRAY.length;j++){
                    if(MyStringUtil.isNotNull(tableColnames.get(i))
                        && !tableColnames.get(i).equals(tablePrimaryKey)
                        && tableColnames.get(i).indexOf(Constant.LIKE_NAME_ARRAY[j])!=-1){
                        String attrName = MyStringUtil.toUnderscoreToCamelCase(tableColnames.get(i));
                        sb.append("@ApiImplicitParam(name = \""+attrName+"\",value = \""+attrComments.get(i)+
                                "\",dataType = \""+attrTypes.get(i)+"\",paramType = \"query\",required = false),\n\t\t");
                        break;
                    }
                }
                for(int j=0;j<Constant.SQL_LIKE_NAME_ARRAY.length;j++){
                    if(MyStringUtil.isNotNull(tableColnames.get(i))
                            && tableColnames.get(i).indexOf(Constant.SQL_LIKE_NAME_ARRAY[j])!=-1){
                        String attrName = MyStringUtil.toUnderscoreToCamelCase(tableColnames.get(i));
                        sb.append("@ApiImplicitParam(name = \""+attrName+"Like\",value = \""+attrComments.get(i)+
                                "（模糊查询）\",dataType = \""+attrTypes.get(i)+"\",paramType = \"query\",required = false),\n\t\t");
                        break;
                    }
                }
            }
        }
        if(MyStringUtil.isNotNull(sb.toString())){
            sb.deleteCharAt(sb.lastIndexOf("\n\t\t"));
        }
        return sb.toString();
    }
}
