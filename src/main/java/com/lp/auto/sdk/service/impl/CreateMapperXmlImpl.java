package com.lp.auto.sdk.service.impl;

import java.util.List;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateMapperXML;
import com.lp.auto.sdk.utils.Constant;
import com.lp.auto.sdk.utils.FileUtil;
import com.lp.auto.sdk.utils.MyStringUtil;
import com.lp.auto.sdk.utils.PropertiesUtil;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateMapperXmlImpl implements CreateMapperXML {

    /**
     * 写文件
     *
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     */
    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo, Connector connector) throws Exception {
        String entityName = entityInfo.getEntityName();
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityName);

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage+"."+ PropertiesUtil.getConfigValue(Constant.KEY_MAPPER_PATH);
        String entityPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);

        String suffixPath = PropertiesUtil.getConfigValue(Constant.KEY_MAPPINGS_PATH).replaceAll("\\.", "\\\\") + "\\" + upperName + "Mapper" + Constant.FILE_SUFFIX_XML;
        String filePath = Constant.getFilePath(projInfo.getProjectName(),
                Constant.PathType.resourcePath, suffixPath);

        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        sb.append("<!DOCTYPE mapper\n");
        sb.append("PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\"\n");
        sb.append("\"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">\n");
        sb.append("<mapper namespace=\"" + packageName + "." + upperName + "Mapper\">\n\n");

        //查询条件
        sb.append(queryWhereCond(entityInfo.getColnames(), entityInfo.getTableColnames()));

        String model = entityInfo.getTableName();
        if (connector.getDbType().equals(Constant.DB_TYPE_SQLSERVER)) {
            sb.append(selectPageListSQLServer(entityInfo,entityPackage));
        } else if (connector.getDbType().equals(Constant.DB_TYPE_MYSQL)) {
            sb.append(selectPageListMySQL(entityInfo,entityPackage));
        } else if (connector.getDbType().equals(Constant.DB_TYPE_DB2)) {
            String schema = connector.getSchema();
            model = "<include refid=\"schema_name\"/>";
            sb.append("\t<sql id=\"schema_name\">" + schema + "." + entityInfo.getTableName() + "</sql>\n\n");
            sb.append(selectPageListDB2(entityInfo, model,entityPackage));
        }
        sb.append(selectAllListSQL(entityInfo, model,entityPackage));
        sb.append(selectListCountSQL(entityInfo, model));
        sb.append(selectByIdSQL(entityInfo, model,entityPackage));
        sb.append(insertSQL(entityInfo, model,entityPackage));
        sb.append(updateSQL(entityInfo, model,entityPackage));
        sb.append(updateSQLByMap(entityInfo, model,entityPackage));
        sb.append(deleteSQL(entityInfo, model));
        sb.append("</mapper>\n");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * where 条件
     *
     * @param colnames
     * @param tableColnames
     * @return
     */
    private String queryWhereCond(List<String> colnames, List<String> tableColnames) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 查询条件 -->\n");
        buffer.append("\t<sql id=\"query_where_cond\">\n");
        if (colnames != null && colnames.size() > 0
                && tableColnames != null && tableColnames.size() > 0
                && tableColnames.size() == colnames.size()) {
            for (int i = 0; i < colnames.size(); i++) {
                for(int j=0;j<Constant.SQL_LIKE_NAME_ARRAY.length;j++){
                    if(MyStringUtil.isNotNull(tableColnames.get(i))
                        && tableColnames.get(i).indexOf(Constant.SQL_LIKE_NAME_ARRAY[j])!=-1){
                        buffer.append("\t\t<if test=\"" + colnames.get(i) + "Like!=null\">\n");
                        buffer.append("\t\t\tand " + tableColnames.get(i) + " LIKE '%${" + colnames.get(i) + "Like}%'\n");
                        buffer.append("\t\t</if>\n");
                    }
                }
                buffer.append("\t\t<if test=\"" + colnames.get(i) + "!=null\">\n");
                buffer.append("\t\t\tand " + tableColnames.get(i) + "=#{" + colnames.get(i) + "}\n");
                buffer.append("\t\t</if>\n");
            }
        }
        buffer.append("\t</sql>\n\n");
        return buffer.toString();
    }

    /**
     * SQLServer分页
     *
     * @param entityInfo
     * @return
     */
    private String selectPageListSQLServer(EntityInfo entityInfo,String entityPackage) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 分页查询 -->\n");
        buffer.append("\t<select id=\"select" + upperName + "List\" parameterType=\"map\" resultType=\"" + entityPackage+"."+upperName + "\">\n");
        buffer.append("\t\tselect top ${pageSize} * from (\n");
        buffer.append("\t\t\tselect *,row_number() over(order by " + entityInfo.getPrimaryKey() + " desc \n");
        buffer.append("\t\t\t\t) as rowNum from " + entityInfo.getTableName() + " where 1=1 \n\n");

        buffer.append("\t\t<include refid=\"query_where_cond\"/>\n\n");

        //buffer.append(parseList(entityInfo.getColnames(),entityInfo.getTableColnames()));
        buffer.append("\t\t) a where a.rowNum &gt; (${pageIndex}-1)*${pageSize}\n");
        buffer.append("\t</select>\n\n");
        return buffer.toString();
    }

    /**
     * MySQL分页
     *
     * @param entityInfo
     * @return
     */
    private String selectPageListMySQL(EntityInfo entityInfo,String entityPackage) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 分页查询 -->\n");
        buffer.append("\t<select id=\"select" + upperName + "List\" parameterType=\"map\" resultType=\"" +entityPackage+"."+ upperName + "\">\n");
        buffer.append("\t\tselect * from " + entityInfo.getTableName() + " where 1=1 \n\n");

        buffer.append("\t\t<include refid=\"query_where_cond\"/>\n\n");

        //buffer.append(parseList(entityInfo.getColnames(),entityInfo.getTableColnames()));
        buffer.append("\t\t<if test=\"sortName!=null\">\n");
        buffer.append("\t\t\torder by ${sortName}\n");
        buffer.append("\t\t</if>\n");

        buffer.append("\t\t<if test=\"sortOrder!=null\">\n");
        buffer.append("\t\t\t${sortOrder}\n");
        buffer.append("\t\t</if>\n");

        buffer.append("\t\t<if test=\"countIndex!=null\">\n");
        buffer.append("\t\t\tlimit  ${countIndex},${pageSize}\n");
        buffer.append("\t\t</if>\n");

        buffer.append("\t</select>\n\n");
        return buffer.toString();

    }

    /**
     * DB2分页
     *
     * @param entityInfo
     * @param model
     * @return
     */
    private String selectPageListDB2(EntityInfo entityInfo, String model,String entityPackage) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 分页查询 -->\n");
        buffer.append("\t<select id=\"select" + upperName + "List\" parameterType=\"map\" resultType=\"" + entityPackage+"."+ upperName + "\">\n");
        buffer.append("\t\tselect * from (\n");
        buffer.append("\t\t\tselect c.*,row_number() over(order by " + entityInfo.getPrimaryKey() + " desc \n");
        buffer.append("\t\t\t\t) as rowNum from " + model + " c where 1=1 \n\n");

        buffer.append("\t\t<include refid=\"query_where_cond\"/>\n\n");

        //buffer.append(parseList(entityInfo.getColnames(),entityInfo.getTableColnames()));
        buffer.append("\t\t) a where a.rowNum &gt; (${pageIndex}-1)*${pageSize} and a.rowNum &lt;= ${pageIndex}*${pageSize} \n");
        buffer.append("\t</select>\n\n");
        return buffer.toString();

    }

    /**
     * 查询总数SQL
     *
     * @param entityInfo
     * @param model
     * @return
     */
    private String selectListCountSQL(EntityInfo entityInfo, String model) {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 查询总数 -->\n");
        buffer.append("\t<select id=\"select" + upperName + "ListCount\" parameterType=\"map\" resultType=\"int\">\n");
        buffer.append("\t\tselect count(1) from " + model + " where 1=1 \n\n");

        buffer.append("\t\t<include refid=\"query_where_cond\"/>\n\n");

        //buffer.append(parseList(entityInfo.getColnames(),entityInfo.getTableColnames()));
        buffer.append("\t</select>\n\n");
        return buffer.toString();
    }

    /**
     * 查询所有SQL
     *
     * @param entityInfo
     * @param model
     * @return
     */
    private String selectAllListSQL(EntityInfo entityInfo, String model,String entityPackage) {

        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 查询所有-->\n");
        buffer.append("\t<select id=\"selectAll" + upperName + "List\" parameterType=\"map\" resultType=\"" + entityPackage+"."+upperName + "\">\n");
        buffer.append("\t\tselect * from " + model + " where 1=1 \n\n");

        buffer.append("\t\t<include refid=\"query_where_cond\"/>\n\n");

        //buffer.append(parseList(entityInfo.getColnames(),entityInfo.getTableColnames()));
        buffer.append("\t\t<if test=\"sortName!=null\">\n");
        buffer.append("\t\t\torder by ${sortName}\n");
        buffer.append("\t\t</if>\n");

        buffer.append("\t\t<if test=\"sortOrder!=null\">\n");
        buffer.append("\t\t\t${sortOrder}\n");
        buffer.append("\t\t</if>\n");

        buffer.append("\t</select>\n\n");
        return buffer.toString();
    }

    /**
     * 添加条件
     *
     * @param colnames
     * @param tableColnames
     * @return
     */
    /*private String parseList(List<String> colnames, List<String> tableColnames) {
        StringBuffer buffer = new StringBuffer();
        if (colnames != null && colnames.size() > 0
                && tableColnames != null && tableColnames.size() > 0
                && tableColnames.size() == colnames.size()) {
            for (int i = 0; i < colnames.size(); i++) {
                buffer.append("\t\t<if test=\"" + colnames.get(i) + "!=null\">\n");
                buffer.append("\t\t\tand " + tableColnames.get(i) + "=#{" + colnames.get(i) + "}\n");
                buffer.append("\t\t</if>\n");
            }
        }
        return buffer.toString();
    }*/

    /**
     * 查询单个SQL
     *
     * @param entityInfo
     * @param model
     * @return
     */
    private String selectByIdSQL(EntityInfo entityInfo, String model,String entityPackage) {
        String upPrimaryKey = MyStringUtil.toUpperCaseFirstOne(entityInfo.getPrimaryKey());
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 根据ID查询 -->\n");
        buffer.append("\t<select id=\"select" + upperName + "By" + upPrimaryKey + "\" parameterType=\"" + entityInfo.getPrimaryKeyType() + "\" resultType=\"" + entityPackage+"."+upperName + "\">\n");
        buffer.append("\t\tselect * from " + model + " where " + entityInfo.getTablePrimaryKey() + " = #{" + entityInfo.getPrimaryKey() + "}\n");
        buffer.append("\t</select>\n\n");
        return buffer.toString();
    }

    /**
     * 删除SQL
     *
     * @param entityInfo
     * @param model
     * @return
     * @throws Exception
     */
    private String deleteSQL(EntityInfo entityInfo, String model) throws Exception {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 删除 -->\n");
        buffer.append("\t<delete id=\"delete" + upperName + "\" parameterType=\"map\">\n");
        buffer.append("\t\tdelete from " + model + " where 1=1\n");
        buffer.append("\t\t<if test=\"" + entityInfo.getPrimaryKey() + "!=null\">\n");
        buffer.append("\t\t\tand " + entityInfo.getTablePrimaryKey() + " = #{" + entityInfo.getPrimaryKey() + "}\n");
        buffer.append("\t\t</if>\n");
        buffer.append("\t\t<if test=\"" + entityInfo.getPrimaryKey() + "s!=null\">\n");
        buffer.append("\t\t\tand " + entityInfo.getTablePrimaryKey() + " in (${" + entityInfo.getPrimaryKey() + "s})\n");
        buffer.append("\t\t</if>\n");
        buffer.append("\t</delete>\n\n");
        return buffer.toString();
    }

    /**
     * 修改SQL
     *
     * @param entityInfo
     * @param model
     * @return
     * @throws Exception
     */
    private String updateSQL(EntityInfo entityInfo, String model,String entityPackage) throws Exception {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 修改 -->\n");
        buffer.append("\t<update id=\"update" + upperName + "\" parameterType=\"" + entityPackage+"."+upperName + "\">\n");
        buffer.append("\t\tupdate  " + model + " \n");
        buffer.append("\t\t<set>\n");
        buffer.append(updateAttribute(entityInfo.getColnames(), entityInfo.getTableColnames(), entityInfo.getTablePrimaryKey()));
        buffer.append("\t\t</set>\n");
        buffer.append("\t\t<where>\n");
        buffer.append("\t\t\t" + entityInfo.getTablePrimaryKey() + "=#{" + entityInfo.getPrimaryKey() + "}\n");
        buffer.append("\t\t</where>\n");
        buffer.append("\t</update>\n\n");
        return buffer.toString();
    }

    /**
     * 修改SQLByMap
     *
     * @param entityInfo
     * @param model
     * @return
     * @throws Exception
     */
    private String updateSQLByMap(EntityInfo entityInfo, String model,String entityPackage) throws Exception {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 修改ByMap -->\n");
        buffer.append("\t<update id=\"update" + upperName + "ByMap\" parameterType=\"map\">\n");
        buffer.append("\t\tupdate  " + model + " \n");
        buffer.append("\t\t<set>\n");
        buffer.append(updateAttributeByMap(entityInfo.getColnames(), entityInfo.getTableColnames(), entityInfo.getTablePrimaryKey()));
        buffer.append("\t\t</set>\n");
        buffer.append("\t\twhere 1=1\n");
        buffer.append("\t\t<include refid=\"query_where_cond\"/>\n");
        buffer.append("\t</update>\n\n");
        return buffer.toString();
    }


    /**
     * 修改SQL语句拼接属性
     *
     * @return
     * @throws Exception
     */
    private static String updateAttribute(List<String> colnames, List<String> tableColnames, String tablePrimaryKey)
            throws Exception {
        StringBuffer sb = new StringBuffer();
        if (colnames != null && colnames.size() > 0
                && tableColnames != null && tableColnames.size() > 0
                && tableColnames.size() == colnames.size()) {
            for (int i = 0; i < colnames.size(); i++) {
                if (!tablePrimaryKey.equals(tableColnames.get(i))) {
                    sb.append("\t\t\t<if test=\"" + colnames.get(i) + "!=null\">\n");
                    sb.append("\t\t\t\t" + tableColnames.get(i) + "=#{" + colnames.get(i) + "},\n");
                    sb.append("\t\t\t</if>\n");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 修改SQL语句拼接属性
     *
     * @return
     * @throws Exception
     */
    private static String updateAttributeByMap(List<String> colnames, List<String> tableColnames, String tablePrimaryKey)
            throws Exception {
        StringBuffer sb = new StringBuffer();
        if (colnames != null && colnames.size() > 0
                && tableColnames != null && tableColnames.size() > 0
                && tableColnames.size() == colnames.size()) {
            for (int i = 0; i < colnames.size(); i++) {
                if (!tablePrimaryKey.equals(tableColnames.get(i))) {
                    sb.append("\t\t\t<if test=\"" + colnames.get(i) + "New!=null\">\n");
                    sb.append("\t\t\t\t" + tableColnames.get(i) + "=#{" + colnames.get(i) + "New},\n");
                    sb.append("\t\t\t</if>\n");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 新增SQL
     *
     * @param entityInfo
     * @param model
     * @return
     * @throws Exception
     */
    private String insertSQL(EntityInfo entityInfo, String model,String entityPackage) throws Exception {
        String upperName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 新增 -->\n");
        String useGeneratedKeys = "";
        if ("Integer".equals(entityInfo.getPrimaryKeyType())||"Long".equals(entityInfo.getPrimaryKeyType())) {
            useGeneratedKeys = " useGeneratedKeys=\"true\" keyProperty=\"" + entityInfo.getPrimaryKey() + "\" keyColumn=\"" + entityInfo.getTablePrimaryKey() + "\"";
        }
        buffer.append("\t<insert id=\"insert" + upperName + "\" parameterType=\"" + entityPackage+"."+upperName + "\"" + useGeneratedKeys + ">\n");
        buffer.append("\t\tinsert into  " + model + " \n");
        buffer.append("\t\t<trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">\n");
        buffer.append(getAttributeStr(entityInfo.getColnames(), entityInfo.getTableColnames(),entityInfo.getPrimaryKeyType()));
        buffer.append("\t\t</trim>\n");

        buffer.append("\t\tvalues\n");

        buffer.append("\t\t<trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">\n");
        buffer.append(getValuesStr(entityInfo.getColnames(), entityInfo.getTableColnames(),entityInfo.getPrimaryKeyType()));
        buffer.append("\t\t</trim>\n");

        buffer.append("\t</insert>\n\n");
        return buffer.toString();
    }


    /**
     * 新增SQL语句拼接属性
     *
     * @return
     * @throws Exception
     */
    private static String getAttributeStr(List<String> colnames, List<String> tableColnames,String primaryKeyType)
            throws Exception {
        StringBuffer sb = new StringBuffer();
        if (colnames != null && colnames.size() > 0
                && tableColnames != null && tableColnames.size() > 0
                && tableColnames.size() == colnames.size()) {
            for (int i = 0; i < colnames.size(); i++) {
                boolean flag = true;
                if(i==0 && ("Integer".equals(primaryKeyType) || "Long".equals(primaryKeyType))){
                    flag = false;
                }
                if(flag ==true){
                    sb.append("\t\t\t<if test=\"" + colnames.get(i) + "!=null\">\n");
                    sb.append("\t\t\t\t" + tableColnames.get(i) + ",\n");
                    sb.append("\t\t\t</if>\n");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 新增SQL语句拼接值
     *
     * @return
     * @throws Exception
     */
    private static String getValuesStr(List<String> colnames, List<String> tableColnames,String primaryKeyType)
            throws Exception {
        StringBuffer sb = new StringBuffer();
        if (colnames != null && colnames.size() > 0
                && tableColnames != null && tableColnames.size() > 0
                && tableColnames.size() == colnames.size()) {
            for (int i = 0; i < colnames.size(); i++) {
                boolean flag = true;
                if(i==0 && ("Integer".equals(primaryKeyType) || "Long".equals(primaryKeyType))){
                    flag = false;
                }
                if(flag ==true){
                    sb.append("\t\t\t<if test=\"" + colnames.get(i) + "!=null\">\n");
                    sb.append("\t\t\t\t#{" + colnames.get(i) + "},\n");
                    sb.append("\t\t\t</if>\n");
                }
            }
        }
        return sb.toString();
    }
}
