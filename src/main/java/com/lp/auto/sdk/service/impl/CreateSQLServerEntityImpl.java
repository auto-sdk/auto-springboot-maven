package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateEntity;
import com.lp.auto.sdk.utils.*;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateSQLServerEntityImpl implements CreateEntity {

    /**
     * 创建Entity
     *
     * @param resultSet  结果集
     * @param entityInfo 实体信息
     * @param projInfo   项目信息
     * @return 返回实体信息
     * @throws Exception
     */
    public EntityInfo writeFile(ResultSet resultSet, EntityInfo entityInfo, ProjInfo projInfo) throws Exception {
        String primaryKey = "";
        String tablePrimaryKey = "";
        String primaryKeyType = "";
        String tableComment = entityInfo.getTableComment();
        String entityName = entityInfo.getEntityName();
        List<String> colnames = new ArrayList<String>();
        List<String> tableColnames = new ArrayList<String>();
        List<String> attrComments = new ArrayList<String>();
        List<String> attrTypes = new ArrayList<String>();

        String defaultPackage = PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = (MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : defaultPackage) + "."
                + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + entityName + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);

        StringBuffer sb = new StringBuffer();

        sb.append("package "+packageName+";\n\n");
        sb.append("import io.swagger.annotations.ApiModel;\n");
        sb.append("import io.swagger.annotations.ApiModelProperty;\n");

        if (tableComment != null && !"".equals(tableComment)) {
            if (tableComment.substring(tableComment.length() - 1).equals("表")) {
                tableComment = tableComment.substring(0, tableComment.length() - 1);
            }
        }

        sb.append("/**\n");
        sb.append(" * " + tableComment + "\n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("@ApiModel(value = \""+tableComment+"\",description = \""+tableComment+"\")\n");
        sb.append("public class " + entityName
                + " { \n\n");
        int count = 1;
        StringBuffer funcsb = new StringBuffer();
        StringBuffer tostrbuff = new StringBuffer();
        tostrbuff.append("\t@Override\n");
        tostrbuff.append("\tpublic String toString() {\n");
        tostrbuff.append("\t\treturn \"" + entityName + " [");

        boolean flag = false;
        while (resultSet.next()) {
            flag = true;
            String colname = resultSet.getString("COLUMN_NAME");
            String lowerColname = MyStringUtil.toUnderscoreToCamelCase(colname);
            String type_name = resultSet.getString("TYPE_NAME");
            String remark = resultSet.getString("value");
            if (type_name != null && !"".equals(type_name)) {
                type_name = type_name.split(" ")[0];
            }
            String lowerType_name = type_name.toLowerCase();
            tostrbuff.append(lowerColname + "=\" + " + lowerColname + " + \",");
            String attrType = "";
            if (count == 1) {
                primaryKey = lowerColname;
                tablePrimaryKey = colname;
                String is_identity = resultSet.getString("is_identity");
                if (!"1".equals(is_identity)) {
                    primaryKeyType = "String";
                } else {
                    primaryKeyType = "Integer";
                }
            }
            count++;

            if ("int".equals(lowerType_name)) {
                attrType="Integer";
            }else if ("bigint".equals(lowerType_name)) {
                attrType = "Long";
            } else if ("varchar".equals(lowerType_name)
                    || "nvarchar".equals(lowerType_name)
                    || "datetime".equals(lowerType_name) || "date".equals(lowerType_name)
                    || "char".equals(lowerType_name) || "time".equals(lowerType_name)) {
                attrType="String";
            } else if ("double".equals(lowerType_name) || "money".equals(lowerType_name)
                    || "decimal".equals(lowerType_name) || "float".equals(lowerType_name)
                    || "numeric".equals(lowerType_name)) {
                attrType="Double";
            } else if ("image".equals(lowerType_name)
                    || "blob".equals(lowerType_name)
                    || "binary".equals(lowerType_name)
                    || "varbinary".equals(lowerType_name)) {
                attrType="byte[]";
            } else {
                if (!"sysname".equals(lowerType_name)) {
                    attrType="String";
                }
            }

            if(MyStringUtil.isNotNull(attrType)){
                sb.append(defineAttr(lowerColname, attrType, remark));
                funcsb.append(getFuncName(lowerColname, attrType));

                attrComments.add(remark==null?"":remark);
                attrTypes.add(attrType);
                colnames.add(lowerColname);
                tableColnames.add(colname);
            }
        }

        if (flag == false) {
            throw new Exception("表" + entityInfo.getTableName() + "不存在");
        }

        entityInfo.setPrimaryKey(primaryKey);
        entityInfo.setPrimaryKeyType(primaryKeyType);
        entityInfo.setTablePrimaryKey(tablePrimaryKey);
        entityInfo.setColnames(colnames);
        entityInfo.setTableColnames(tableColnames);
        entityInfo.setAttrTypes(attrTypes);
        entityInfo.setAttrComments(attrComments);

        tostrbuff.deleteCharAt(tostrbuff.lastIndexOf(","));
        tostrbuff.append("]\";\n");
        tostrbuff.append("\t}\n");
        sb.append(funcsb.toString());
        sb.append(tostrbuff.toString());
        sb.append("}\n");
        FileUtil.createFile(filePath, sb.toString(), false);
        return entityInfo;
    }

    /**
     * 设置实体类的属性
     *
     * @param name    属性名
     * @param type    数据类型
     * @param comment 注释
     * @return
     */
    private String defineAttr(String name, String type, String comment) {
        comment = comment == null ? "" : comment;
        String namestr = "";
        if(type.equals("Integer") || type.equals("Double")){
            namestr = "\t@ApiModelProperty(value = \""+comment+"\",example=\"1\")\n";
        }else{
            namestr = "\t@ApiModelProperty(value = \""+comment+"\")\n";
        }
        namestr+= "\tprivate " + type + " "+ MyStringUtil.toLowerCaseFirstOne(name) + ";\n\n";
        return namestr;
    }

    /**
     * 设置get与set方法
     *
     * @param name
     * @param type
     * @return
     */
    private String getFuncName(String name, String type) {
        StringBuffer sb = new StringBuffer();
        String lowerName = MyStringUtil.toLowerCaseFirstOne(name);
        String upperName = MyStringUtil.toUpperCaseFirstOne(name);
        sb.append("\tpublic ");
        sb.append(type);
        sb.append(" get");
        sb.append(upperName);
        sb.append("() {\n\t\treturn ");
        sb.append(lowerName);
        sb.append(";\n\t}\n\n");

        sb.append("\tpublic void set");
        sb.append(upperName);
        sb.append("(");
        sb.append(type);
        sb.append(" ");
        sb.append(lowerName);
        sb.append(") { \n\t\tthis.");
        sb.append(lowerName);
        sb.append(" = ");
        sb.append(lowerName);
        sb.append(";\n\t}\n\n");
        return sb.toString();
    }
}
