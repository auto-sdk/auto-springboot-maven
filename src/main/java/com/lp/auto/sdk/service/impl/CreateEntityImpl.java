package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.service.CreateEntity;
import com.lp.auto.sdk.utils.Constant;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateEntityImpl {

    public static CreateEntity getCreateEntity(String dbType) {
        CreateEntity createEntity = null;
        if (Constant.DB_TYPE_DB2.equals(dbType)) {
            createEntity = new CreateDB2EntityImpl();
        } else if (Constant.DB_TYPE_SQLSERVER.equals(dbType)) {
            createEntity = new CreateSQLServerEntityImpl();
        } else if (Constant.DB_TYPE_MYSQL.equals(dbType)) {
            createEntity = new CreateMySQLEntityImpl();
        }
        return createEntity;
    }
}
