package com.lp.auto.sdk.service.impl;

import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.service.CreateService;
import com.lp.auto.sdk.utils.*;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class CreateServiceFileImpl implements CreateService {

    public void writeFile(EntityInfo entityInfo, ProjInfo projInfo) throws Exception {

        String tableComment = entityInfo.getTableComment();
        String lowerPrimaryKey = MyStringUtil.toLowerCaseFirstOne(entityInfo.getPrimaryKey());
        String upperPrimaryKey = MyStringUtil.toUpperCaseFirstOne(entityInfo.getPrimaryKey());
        String primaryKeyType = entityInfo.getPrimaryKeyType();

        String upperEntityName = MyStringUtil.toUpperCaseFirstOne(entityInfo.getEntityName());
        String lowerEntityName = MyStringUtil.toLowerCaseFirstOne(entityInfo.getEntityName());

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);
        String entityPackName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String utilPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);


        String dirPath = PublicFileUtil.class.getClassLoader().getResource("").getPath();
        try {
            dirPath = URLDecoder.decode(dirPath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String srcFilePath = dirPath.replace("/target/test-classes/", "/target/classes/")+Constant.TEMPLATE_SERVICE_PATH;

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + upperEntityName + "Service" + Constant.FILE_SUFFIX_JAVA;
        String destFilePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);

        Map<String,String> map = new HashMap<String,String>(){
            {
                put("FILE_PACKAGE_NAME",packageName);
                put("ENTITY_PACKAGE",entityPackName);
                put("UPPER_ENTITY_NAME",upperEntityName);
                put("TABLE_COMMENT",tableComment);
                put("PROJECT_AUTHOR",projInfo.getAuthor());
                put("NOW_DATE",DateUtil.getNowDate(DateUtil.DATE_PATTERN));
                put("LOWER_ENTITY_NAME",lowerEntityName);
                put("UPPER_PRIMARY_KEY",upperPrimaryKey);
                put("LOWER_PRIMARY_KEY",lowerPrimaryKey);
                put("UTILS_PACKAGE",utilPackage);
            }
        };
        FileUtil.copyFile(srcFilePath, destFilePath, map);
    }
}
