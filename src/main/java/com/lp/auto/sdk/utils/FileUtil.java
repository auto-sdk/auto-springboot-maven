package com.lp.auto.sdk.utils;

import com.alibaba.fastjson.JSONObject;
import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.ProjInfo;

import java.io.*;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-06-30
 **/
public class FileUtil {

    private static Map<String,Map<String,String>> fileNames;

    /**
     * 创建文件
     *
     * @param filePath  文件路径
     * @param content   写入内容
     * @param coverFlag 存在是否覆盖
     */
    public static void createFile(String filePath, String content, boolean coverFlag) throws IOException {
    	File file = null;
    	String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
        //如果是生成zip
        if("true".equals(createZipFlag)){
        	file = new File(filePath);
        }else{
        	String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
        	filePath = filePath.replace(rootDir, "");
        	file = new File(filePath);
            if (coverFlag == false) {
                if (file.exists()) {
                    System.out.println(Constant.warnMsg + filePath + " 已存在");
                    return;
                }
            }
        }

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        System.out.println(DateUtil.formatDate(new Date(), DateUtil.TIMESTAMP_PATTERN) + " " + filePath);
        OutputStream oos = new FileOutputStream(file);
        oos.write(content.getBytes("utf-8"));
        oos.flush();
        oos.close();
    }

    //一行一行读写
    public static void copyFile(String srcFilePath, String destFilePath) {
        try {
            FileReader reader = new FileReader(srcFilePath);
            BufferedReader bufferedReader = new BufferedReader(reader);

            File outFile = new File(destFilePath);
            if (!outFile.getParentFile().exists()) {
                outFile.getParentFile().mkdirs();
            }
            FileWriter writer = new FileWriter(outFile);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            writer.close();
            reader.close();
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 替换文件中的内容，复制文件
     * @param srcFilePath
     * @param destFilePath
     * @param map
     */
    public static void copyFile(String srcFilePath, String destFilePath,Map<String,String> map){

        String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
        //如果是生成zip
        if(!"true".equals(createZipFlag)){
            String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
            destFilePath = destFilePath.replace(rootDir, "");

        }

        File file = new File(destFilePath);
        if (file.exists()) {
            System.out.println(Constant.warnMsg + destFilePath + " 已存在");
            return;
        }

        System.out.println(DateUtil.formatDate(new Date(), DateUtil.TIMESTAMP_PATTERN) + " " + destFilePath);
        try {
            FileReader reader = new FileReader(srcFilePath);
            BufferedReader bufferedReader = new BufferedReader(reader);

            File outFile = new File(destFilePath);
            if (!outFile.getParentFile().exists()) {
                outFile.getParentFile().mkdirs();
            }
            FileWriter writer = new FileWriter(outFile);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {

                for(Map.Entry<String, String> entry:map.entrySet()){
                    if (line.indexOf(entry.getKey()) != -1) {
                        line = line.replaceAll(entry.getKey(),entry.getValue());
                    }
                }
                bufferedWriter.write(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            writer.close();
            reader.close();
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("serial")
    private static void initFileNames(final ProjInfo projInfo, final Connector connector){

        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        final String controllerPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_CONTROLLER_PATH);
        final String utilPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        final String annotationPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ANNOTATION_PATH);
        final String filterPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_FILTER_PATH);
        final String interceptorPackage = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_INTERCEPTOR_PATH);
        final String mappingPath = PropertiesUtil.getConfigValue(Constant.KEY_MAPPINGS_PATH);
        final String mapperPackage = basePackage+"."+PropertiesUtil.getConfigValue(Constant.KEY_MAPPER_PATH);
        final String entityPackage = basePackage+"."+PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        final String accessBaseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        final String configJavaPackage = basePackage+"."+PropertiesUtil.getConfigValue(Constant.CONFIG_JAVA_PATH);

        fileNames= new HashMap<String, Map<String,String>>(){
            {

                put("SysLog.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",annotationPackage);
                    }
                });

                put("LogAspect.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",annotationPackage);
                    }
                });

                put("BaseController.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",controllerPackage);
                    }
                });

                put("CorsFilter.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",filterPackage);
                        put("UTIL_PACKAGE_NAME",utilPackage);
                    }
                });

                put("BodyReaderHttpServletRequestWrapper.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("ManageInterceptor.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",interceptorPackage);
                        put("UTIL_PACKAGE",utilPackage);
                    }
                });

                put("ConfigProperties.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                        put("CONFIG_FILE_PATH","config/config.properties");
                    }
                });

                put("ConstantUtil.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                        put("PROJECT_NAME",projInfo.getProjectName());
                    }
                });

                put("DaoException.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("DateUtil.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("FilterConfig.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",configJavaPackage);
                        put("FILTER_PACKAGE",filterPackage);
                    }
                });

                put("HttpClient.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("InitialTask.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("InterceptorConfig.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",configJavaPackage);
                        put("INTERCEPTOR_PACKAGE",interceptorPackage);
                        put("ACCESS_BASE_URL",accessBaseUrl);
                    }
                });

                put("MyControllerAdvice.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                        put("CONTROLLER_PACKAGE",controllerPackage);
                    }
                });

                put("MD5Utils.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("RedisConfig.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",configJavaPackage);
                    }
                });

                put("RedisUtils.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("RegexUtil.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("ResultEntity.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("SpringContextUtil.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("StringUtil.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("Swagger2Config.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",configJavaPackage);
                        put("CONTROLLER_PACKAGE",controllerPackage);
                        put("PROJECT_NAME",projInfo.getProjectName());
                        put("PROJECT_AUTHOR",projInfo.getAuthor());
                    }
                });

                put("ValidateException.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("ExportExcel.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                    }
                });

                put("ExceptionHandler.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",utilPackage);
                        put("CONTROLLER_PACKAGE",controllerPackage);
                    }
                });

                put("TemplateApplication.java", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",basePackage);
                        put("MAPPER_PACKAGE",mapperPackage);
                        String fileName = MyStringUtil.toUpperCaseFirstOne(MyStringUtil.toUnderscoreToCamelCase(projInfo.getProjectName()))
                                +"Application";
                        put("APPLICATION_FILE_NAME",fileName);
                    }
                });

                put("template_pom.xml", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME","pom.xml");
                        put("PROJECT_PACKAGE_NAME",projInfo.getPackageName());
                        put("PROJECT_NAME",projInfo.getProjectName());

                        String fileName = MyStringUtil.toUpperCaseFirstOne(MyStringUtil.toUnderscoreToCamelCase(projInfo.getProjectName()))
                                +"Application";

                        put("APPLIACTION_CLASS",fileName);
                    }
                });

                put(".template_gitignore", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME",".gitignore");
                    }
                });

                put("template_application-test.properties", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME","application-test.properties");
                        put("PROJECT_NAME",projInfo.getProjectName());
                        put("ENTITY_PACKAGE",entityPackage);
                        put("MAPPINGS_PATH",mappingPath);
                        put("DB_URL",connector.getUrl());
                        put("DB_USERNAME",connector.getUser());
                        put("DB_PASSWORD",connector.getPassword());
                        put("DB_DRIVER",connector.getDriver());
                        put("BASE_PACKAGE",basePackage);
                    }
                });

                put("template_application-dev.properties", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME","application-dev.properties");
                        put("PROJECT_NAME",projInfo.getProjectName());
                        put("ENTITY_PACKAGE",entityPackage);
                        put("MAPPINGS_PATH",mappingPath);
                        put("DB_URL",connector.getUrl());
                        put("DB_USERNAME",connector.getUser());
                        put("DB_PASSWORD",connector.getPassword());
                        put("DB_DRIVER",connector.getDriver());
                        put("BASE_PACKAGE",basePackage);
                    }
                });

                put("template_application-prod.properties", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME","application-prod.properties");
                        put("PROJECT_NAME",projInfo.getProjectName());
                        put("ENTITY_PACKAGE",entityPackage);
                        put("MAPPINGS_PATH",mappingPath);
                        put("DB_URL",connector.getUrl());
                        put("DB_USERNAME",connector.getUser());
                        put("DB_PASSWORD",connector.getPassword());
                        put("DB_DRIVER",connector.getDriver());
                        put("BASE_PACKAGE",basePackage);
                    }
                });

                put("template_application.properties", new HashMap<String,String>(){
                    {
                        put("FILE_PACKAGE_NAME","application.properties");
                    }
                });
            }
        };
    }

    public static void copyPublicFiles(ProjInfo projInfo,Connector connector){
        initFileNames(projInfo,connector);

        String dirPath = PublicFileUtil.class.getClassLoader().getResource("").getPath();
        try {
            dirPath = URLDecoder.decode(dirPath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        dirPath = dirPath.replace("/target/test-classes/", "/target/classes/")+"template/";

        File file = new File(dirPath);
        writeFile(file, projInfo);
    }

    public static void writeFile(File sourceFile,ProjInfo projInfo){
        if(sourceFile.exists() && sourceFile.isDirectory()){
            File[] files = sourceFile.listFiles();
            if(files.length>0){
                for(int i=0;i<files.length;i++){
                    if(files[i].isFile()){
                        Map<String,String> map = fileNames.get(files[i].getName());
                        if(map!=null && map.containsKey("FILE_PACKAGE_NAME")){
                            String packageName = map.get("FILE_PACKAGE_NAME");

                            String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);

                            String outFilePath = "";
                            if(files[i].getName().indexOf(Constant.FILE_SUFFIX_JAVA)!=-1){

                                if(files[i].getName().indexOf("TemplateApplication.java")!=-1){
                                    String fileName = map.get("APPLICATION_FILE_NAME")+Constant.FILE_SUFFIX_JAVA;
                                    String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + fileName;
                                    outFilePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
                                }else{
                                    String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + files[i].getName();
                                    outFilePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
                                }
                            }else if(files[i].getName().indexOf("pom.xml")!=-1){
                                outFilePath = Constant.getFilePath(projInfo.getProjectName(), null, packageName);
                            }else if(files[i].getName().indexOf("gitignore")!=-1){
                                outFilePath = Constant.getFilePath(projInfo.getProjectName(), null, packageName);
                            }else{
                                String suffixPath = packageName.replaceAll("\\/", "\\\\");
                                outFilePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.resourcePath, suffixPath);
                            }

                            String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
                            boolean flag = true;

                            File descFile = null;
                            //如果是生成zip
                            if(!"true".equals(createZipFlag)){
                                outFilePath = outFilePath.replace(rootDir, "");
                                descFile = new File(outFilePath);
                                if (descFile.exists()) {
                                    System.out.println(Constant.warnMsg + outFilePath + " 已存在");
                                    flag = false;
                                }
                            }else{
                                descFile = new File(outFilePath);
                            }

                            if(flag == true){
                                if (!descFile.getParentFile().exists()) {
                                    descFile.getParentFile().mkdirs();
                                }
                                System.out.println(DateUtil.formatDate(new Date(), DateUtil.TIMESTAMP_PATTERN) + " " + outFilePath);

                                try {
                                    FileReader reader = new FileReader(files[i].getAbsolutePath());
                                    BufferedReader bufferedReader = new BufferedReader(reader);

                                    File outFile = new File(outFilePath);
                                    if (!outFile.getParentFile().exists()) {
                                        outFile.getParentFile().mkdirs();
                                    }
                                    FileWriter writer = new FileWriter(outFile);
                                    BufferedWriter bufferedWriter = new BufferedWriter(writer);

                                    String line = "";
                                    while ((line = bufferedReader.readLine()) != null) {

                                        for(Map.Entry<String, String> entry:map.entrySet()){
                                            if (line.indexOf(entry.getKey()) != -1) {
                                                line = line.replaceAll(entry.getKey(),entry.getValue());
                                            }
                                        }
                                        bufferedWriter.write(line);
                                        bufferedWriter.newLine();
                                        bufferedWriter.flush();
                                    }
                                    writer.close();
                                    reader.close();
                                    bufferedReader.close();
                                    bufferedWriter.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }else if(files[i].isDirectory()){
                        writeFile(files[i], projInfo);
                    }
                }
            }
        }
    }
}
