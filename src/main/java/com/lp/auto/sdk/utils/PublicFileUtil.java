package com.lp.auto.sdk.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.ProjInfo;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class PublicFileUtil {

    /**
     * 创建BaseController.java
     *
     * @param dbType
     * @param projInfo
     */
    public static void createBaseController(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_CONTROLLER_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "BaseController" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package "+packageName+";\n\n");

        sb.append("import java.util.HashMap;\n");
        sb.append("import java.util.Map;\n");
        sb.append("import java.util.Map.Entry;\n\n");

        sb.append("import javax.servlet.http.HttpServletRequest;\n");
        sb.append("import org.apache.commons.lang.StringUtils;\n");
        sb.append("import org.springframework.stereotype.Controller;\n\n");

        sb.append("/**\n");
        sb.append(" * 基础工具控制器\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("@Controller\n");
        sb.append("public class BaseController {\n");

        sb.append("\t/**\n");
        sb.append("\t * 初始化request\n");
        sb.append("\t */\n");
        sb.append("\tprotected Map<String, Object> initRequestMap(HttpServletRequest request) {\n");
        sb.append("\t\tMap<String, Object> map = new HashMap<String, Object>();\n");
        sb.append("\t\tMap<String,String[]> params = request.getParameterMap();\n");
        sb.append("\t\tif(params != null) {\n");
        sb.append("\t\t\tfor(Entry<String,String[]> entry:params.entrySet()){\n");
        sb.append("\t\t\t\tString key = entry.getKey();\n");
        sb.append("\t\t\t\tString [] value = entry.getValue();\n");
        sb.append("\t\t\t\tif(StringUtils.isNotBlank(key)\n");
        sb.append("\t\t\t\t\t&& value != null && value.length > 0) {\n");
        sb.append("\t\t\t\t\tif(StringUtils.isNotBlank(value[0])){\n");
        sb.append("\t\t\t\t\t\tmap.put(key, value[0]);\n");
        sb.append("\t\t\t\t\t}\n");
        sb.append("\t\t\t\t}\n");
        sb.append("\t\t\t}\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn map;\n");
        sb.append("\t}\n\n");

        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }


    /**
     * 创建DaoException.java文件
     *
     * @throws IOException
     */
    public static void createDaoExceptionFile(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "DaoException" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package "+packageName+";\n\n");

        sb.append("/**\n");
        sb.append(" * Daoy异常类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("public class DaoException extends Exception {\n\n");
        sb.append("\tprivate static final long serialVersionUID = 1L;\n\n");
        sb.append("\tprivate String message;\n");
        sb.append("\tprivate Exception exception;\n\n");
        sb.append("\tpublic DaoException() {\n");
        sb.append("\t\tif (message != null && !\"\".equals(message)) {\n");
        sb.append("\t\t\tSystem.out.println(message);\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic DaoException(Exception exception, String message) {\n");
        sb.append("\t\tthis.message = message;\n");
        sb.append("\t\tthis.exception = exception;\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic DaoException(String message) {\n");
        sb.append("\t\tthis.message = message;\n");
        sb.append("\t}\n\n");

        sb.append("\t@Override\n");
        sb.append("\tpublic String getMessage() {\n");
        sb.append("\t\treturn message;\n");
        sb.append("\t}\n");

        sb.append("\t@Override\n");
        sb.append("\tpublic void printStackTrace() {\n");
        sb.append("\t\tSystem.out.println(message);\n");
        sb.append("\t\tif (exception != null) {\n");
        sb.append("\t\t\texception.printStackTrace();\n");
        sb.append("\t\t} else {\n");
        sb.append("\t\t\tsuper.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }


    /**
     * 创建ValidateException.java文件
     *
     * @throws IOException
     */
    public static void createValidateExceptionFile(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "ValidateException" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package "+packageName+";\n\n");

        sb.append("/**\n");
        sb.append(" * 校验异常类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("public class ValidateException extends Exception {\n\n");
        sb.append("\tprivate static final long serialVersionUID = 1L;\n\n");
        sb.append("\tprivate String message;\n");
        sb.append("\tprivate Exception exception;\n\n");
        sb.append("\tpublic ValidateException() {\n");
        sb.append("\t\tif (message != null && !\"\".equals(message)) {\n");
        sb.append("\t\t\tSystem.out.println(message);\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic ValidateException(Exception exception, String message) {\n");
        sb.append("\t\tthis.message = message;\n");
        sb.append("\t\tthis.exception = exception;\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic ValidateException(String message) {\n");
        sb.append("\t\tthis.message = message;\n");
        sb.append("\t}\n\n");

        sb.append("\t@Override\n");
        sb.append("\tpublic String getMessage() {\n");
        sb.append("\t\treturn message;\n");
        sb.append("\t}\n");

        sb.append("\t@Override\n");
        sb.append("\tpublic void printStackTrace() {\n");
        sb.append("\t\tSystem.out.println(message);\n");
        sb.append("\t\tif (exception != null) {\n");
        sb.append("\t\t\texception.printStackTrace();\n");
        sb.append("\t\t} else {\n");
        sb.append("\t\t\tsuper.printStackTrace();\n");
        sb.append("\t\t}\n");
        sb.append("\t}\n\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建BaseService.java文件
     *
     * @throws IOException
     */
    public static void createBaseServiceFile(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);
        String utilPackageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "BaseService" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("package "+packageName+";\n\n");

        sb.append("import org.springframework.beans.factory.annotation.Autowired;\n\n");
        sb.append("import org.apache.ibatis.session.SqlSession;\n");
        sb.append("import org.springframework.stereotype.Component;\n\n");
        sb.append("import " + utilPackageName + ".DataSourceContextHolder;\n\n");

        sb.append("/**\n");
        sb.append(" * SqlSession基础类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("@Component\n");
        sb.append("public class BaseService {\n\n");
        sb.append("\tprivate SqlSession sqlSession;\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 获取sqlSession\n");
        sb.append("\t * \n");
        sb.append("\t * @return\n");
        sb.append("\t */\n");
        sb.append("\tpublic SqlSession getSqlSession() {\n");
        sb.append("\t\tDataSourceContextHolder.setDbType(DataSourceContextHolder.dataSource);\n");
        sb.append("\t\treturn sqlSession;\n");
        sb.append("\t}\n\n");
        sb.append("\t@Autowired\n");
        sb.append("\tpublic void setSqlSession(SqlSession sqlSession) {\n");
        sb.append("\t\tthis.sqlSession = sqlSession;\n");
        sb.append("\t}\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建db.properties文件
     *
     * @throws IOException
     */
    public static void createDBProFile(ProjInfo projInfo, Connector connector) throws IOException {
        String suffixPath = "db" + Constant.FILE_SUFFIX_PROPERTIES;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.resource_db, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("SQL_DRIVER=" + connector.getDriver() + "\n");
        sb.append("SQL_URL=" + connector.getUrl() + "\n");
        sb.append("SQL_USERNAME=" + connector.getUser() + "\n");
        sb.append("SQL_PASSWORD=" + connector.getPassword() + "\n");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建log4j.properties文件
     *
     * @throws IOException
     */
    public static void createLog4jFile(ProjInfo projInfo) throws IOException {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String suffixPath = "log4j" + Constant.FILE_SUFFIX_PROPERTIES;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.resourcePath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("# Global logging configuration\n");
        sb.append("#debug info warn error\n");
        sb.append("log4j.rootLogger=warn,stdout,file\n");
        sb.append("# MyBatis logging configuration...\n");
        sb.append("log4j.appender.stdout=org.apache.log4j.ConsoleAppender\n");
        sb.append("log4j.appender.stdout.layout=org.apache.log4j.PatternLayout\n");
        sb.append("log4j.appender.stdout.layout.ConversionPattern=%5p [%t] - %m%n\n");
        sb.append("log4j.appender.file.File=logs/" + projInfo.getProjectName() + ".log\n");
        sb.append("log4j.appender.file=org.apache.log4j.FileAppender\n");
        sb.append("log4j.appender.file.layout=org.apache.log4j.PatternLayout\n");
        sb.append("log4j.appender.file.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss}  %l  %m%n\n\n");
        sb.append("log4j.logger." + basePackage + "=debug\n");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建ConstantUtil.java 文件
     */
    public static void createConstantUtilFile(ProjInfo projInfo) throws IOException {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "ConstantUtil" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();
        sb.append("package "+packageName+";\n\n");

        sb.append("/**\n");
        sb.append(" * 定义常量类\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("public class ConstantUtil {\n\n");

        sb.append("\t/**\n");
        sb.append("\t * 错误代号值\n");
        sb.append("\t */\n");
        sb.append("\tpublic final static String CODE_200=\"200\"; //操作成功\n");
        sb.append("\tpublic final static String CODE_401=\"401\"; //验证码错误\n");
        sb.append("\tpublic final static String CODE_402=\"402\"; //用户名或密码错误\n");
        sb.append("\tpublic final static String CODE_403=\"403\"; //没有操作权限\n");
        sb.append("\tpublic final static String CODE_404=\"404\"; //参数错误\n");
        sb.append("\tpublic final static String CODE_406=\"406\"; //账号在另一地点登录\n");
        sb.append("\tpublic final static String CODE_407=\"407\"; //该值已经存在\n");
        sb.append("\tpublic final static String CODE_500=\"500\"; //系统异常\n\n");
        sb.append("\t/**\n");
        sb.append("\t * 错误消息\n");
        sb.append("\t */\n");
        sb.append("\tpublic final static String MSG_ADD_SUCCESS=\"新增成功\";\n");
        sb.append("\tpublic final static String MSG_EDIT_SUCCESS=\"修改成功\";\n");
        sb.append("\tpublic final static String MSG_DEL_SUCCESS=\"删除成功\";\n");
        sb.append("\tpublic final static String MSG_ADD_ERROR=\"新增失败\";\n");
        sb.append("\tpublic final static String MSG_EDIT_ERROR=\"修改失败\";\n");
        sb.append("\tpublic final static String MSG_DEL_ERROR=\"删除失败\";\n");
        sb.append("\tpublic final static String MSG_SELECT_ERROR=\"查询失败\";\n");
        sb.append("\tpublic final static String MSG_PAGE_DATA_ERROR=\"分页参数错误\";\n");
        sb.append("\tpublic final static String MSG_401=\"验证码错误\";\n");
        sb.append("\tpublic final static String MSG_402=\"用户名或密码错误\";\n");
        sb.append("\tpublic final static String MSG_403=\"没有操作权限\";\n");
        sb.append("\tpublic final static String MSG_404=\"参数错误\";\n");
        sb.append("\tpublic final static String MSG_406=\"您的账号在另一地点登录\";\n");
        sb.append("\tpublic final static String MSG_500=\"系统异常\";\n\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建ResultEntity.java 文件
     */
    public static void createResultEntityFile(ProjInfo projInfo) throws IOException {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "ResultEntity" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();
        sb.append("package "+packageName+";\n\n");

        sb.append("import com.alibaba.fastjson.JSON;\n");
        sb.append("import com.alibaba.fastjson.JSONObject;\n\n");

        sb.append("/**\n");
        sb.append(" * JSON数据模板\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("public class ResultEntity<T> {\n\n");
        sb.append("\tprivate T result;\n\n");
        sb.append("\tprivate String msg;\n\n");
        sb.append("\tprivate Integer total;\n\n");
        sb.append("\tprivate String error_code;\n\n");
        sb.append("\tprivate int flag = 0;\n\n");
        sb.append("\tpublic ResultEntity(String error_code, String msg) {\n");
        sb.append("\t\tthis.msg = msg;\n");
        sb.append("\t\tthis.error_code = error_code;\n");
        sb.append("\t\tflag = 1;\n");
        sb.append("\t}\n\n");
        sb.append("\tpublic ResultEntity(String error_code, T result) {\n");
        sb.append("\t\tthis.result = result;\n");
        sb.append("\t\tthis.error_code = error_code;\n");
        sb.append("\t\tflag = 2;\n");
        sb.append("\t}\n\n");
        sb.append("\tpublic ResultEntity(String error_code, T result, Integer total) {\n");
        sb.append("\t\tthis.result = result;\n");
        sb.append("\t\tthis.error_code = error_code;\n");
        sb.append("\t\tthis.total = total;\n");
        sb.append("\t\tflag = 3;\n");
        sb.append("\t}\n\n");
        sb.append(getFuncName("result", "T"));
        sb.append(getFuncName("msg", "String"));
        sb.append(getFuncName("total", "Integer"));
        sb.append(getFuncName("error_code", "String"));
        sb.append("\t@Override\n");
        sb.append("\tpublic String toString() {\n");
        sb.append("\t\tJSONObject jsonObject = (JSONObject) JSON.toJSON(this);\n");
        sb.append("\t\tif (flag == 1) {\n");
        sb.append("\t\t\tjsonObject.remove(\"result\");\n");
        sb.append("\t\t\tjsonObject.remove(\"total\");\n");
        sb.append("\t\t} else if (flag == 2) {\n");
        sb.append("\t\t\tjsonObject.remove(\"total\");\n");
        sb.append("\t\t\tjsonObject.remove(\"msg\");\n");
        sb.append("\t\t} else if (flag == 3) {\n");
        sb.append("\t\t\tjsonObject.remove(\"msg\");\n");
        sb.append("\t\t}\n");
        sb.append("\t\treturn jsonObject.toString();\n");
        sb.append("\t}\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 设置get与set方法
     *
     * @param name
     * @param type
     * @return
     */
    private static String getFuncName(String name, String type) {
        StringBuffer sb = new StringBuffer();
        String lowerName = MyStringUtil.toLowerCaseFirstOne(name);
        String upperName = MyStringUtil.toUpperCaseFirstOne(name);
        sb.append("\tpublic "+type+" get"+upperName+"() {\n");
        sb.append("\t\treturn "+lowerName+";\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic void set"+upperName+"("+type+" "+lowerName+") { \n");
        sb.append("\t\tthis."+lowerName+" = "+lowerName+";\n");
        sb.append("\t}\n\n");
        return sb.toString();
    }

    /**
     * 创建DataSourceContextHolder.java文件
     *
     * @throws IOException
     */
    public static void createDataSourceHoderFile(ProjInfo projInfo) throws IOException {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "DataSourceContextHolder" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();
        sb.append("package "+packageName+";\n\n");

        sb.append("/**\n");
        sb.append(" * 数据源上下文设置\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("public class DataSourceContextHolder {\n\n");

        sb.append("\tpublic static final String dataSource = \"dataSource\";\n\n");
        sb.append("\tprivate static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();\n\n");
        sb.append("\tpublic static void setDbType(String dbType) {\n");
        sb.append("\t\tcontextHolder.set(dbType);\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic static String getDbType() {\n");
        sb.append("\t\treturn ((String) contextHolder.get());\n");
        sb.append("\t}\n\n");

        sb.append("\tpublic static void clearDbType() {\n");
        sb.append("\t\tcontextHolder.remove();\n");
        sb.append("\t}\n");
        sb.append("}");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建DynamicDataSource.java文件
     *
     * @throws IOException
     */
    public static void createDynamicDataSourceFile(ProjInfo projInfo) throws IOException {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "DynamicDataSource" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
        StringBuffer sb = new StringBuffer();
        sb.append("package "+packageName+";\n\n");

        sb.append("import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;\n\n");
        sb.append("/**\n");
        sb.append(" * 动态数据源配置\n");
        sb.append(" * \n");
        sb.append(" * @author " + projInfo.getAuthor() + "\n");
        sb.append(" * @version 1.0\n");
        sb.append(" * @date " + DateUtil.formatDate(new Date(), DateUtil.DATE_PATTERN) + "\n");
        sb.append(" */\n");
        sb.append("public class DynamicDataSource extends AbstractRoutingDataSource {\n\n");
        sb.append("\t@Override\n");
        sb.append("\tprotected Object determineCurrentLookupKey() {\n");
        sb.append("\t\treturn DataSourceContextHolder.getDbType();\n");
        sb.append("\t}\n");
        sb.append("}");
        FileUtil.createFile(filePath, sb.toString(), false);
    }


    /**
     * 创建spring-mvc.xml文件
     *
     * @throws Exception
     */
    public static void createSpringMVCFile(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String suffixPath = "spring-mvc" + Constant.FILE_SUFFIX_XML;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.resource_spring, suffixPath);
        String accessBaseUrl = PropertiesUtil.getConfigValue(Constant.KEY_ACCESS_BASE_URL);
        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        sb.append("<beans xmlns=\"http://www.springframework.org/schema/beans\"\n");
        sb.append("\txmlns:context=\"http://www.springframework.org/schema/context\"\n");
        sb.append("\txmlns:tx=\"http://www.springframework.org/schema/tx\"\n");
        sb.append("\txmlns:mvc=\"http://www.springframework.org/schema/mvc\"\n");
        sb.append("\txmlns:aop=\"http://www.springframework.org/schema/aop\"\n");
        sb.append("\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
        sb.append("\txsi:schemaLocation=\"http://www.springframework.org/schema/beans\n");
        sb.append("\t\thttp://www.springframework.org/schema/beans/spring-beans-4.3.xsd\n");
        sb.append("\t\thttp://www.springframework.org/schema/context\n");
        sb.append("\t\thttp://www.springframework.org/schema/context/spring-context-4.3.xsd\n");
        sb.append("\t\thttp://www.springframework.org/schema/aop\n");
        sb.append("\t\thttp://www.springframework.org/schema/aop/spring-aop-4.3.xsd\n");
        sb.append("\t\thttp://www.springframework.org/schema/tx\n");
        sb.append("\t\thttp://www.springframework.org/schema/tx/spring-tx-4.3.xsd\n");
        sb.append("\t\thttp://www.springframework.org/schema/mvc\n");
        sb.append("\t\thttp://www.springframework.org/schema/mvc/spring-mvc-4.3.xsd\">\n");

        sb.append("\t<!-- 事务 注解驱动 -->\n");
        sb.append("\t<tx:annotation-driven/>\n\n");
        sb.append("\t<!-- 注解驱动 -->\n");
        sb.append("\t<mvc:annotation-driven />\n\n");

        sb.append("\t<!-- 加载静态资源，包括 html,css,js,img等 -->\n");
        sb.append("\t<mvc:default-servlet-handler />\n\n");
        
        sb.append("\t<!-- AOP配置 -->\n");
        sb.append("\t<aop:aspectj-autoproxy />\n\n");

        sb.append("\t<!-- 配置@Contrller,@Service @Resource @Autowired @Component扫描 -->\n");
        sb.append("\t<context:component-scan base-package=\"" + basePackage + "\">\n");
        sb.append("\t\t<context:include-filter type=\"annotation\" expression=\"org.springframework.stereotype.Controller\" />\n");
        sb.append("\t\t<context:exclude-filter type=\"annotation\" expression=\"org.springframework.stereotype.Service\" />\n");
        sb.append("\t</context:component-scan>\n\n");

        String prefix = PropertiesUtil.getConfigValue(Constant.KEY_MVC_VIEW_PREFIX);
        String suffix = PropertiesUtil.getConfigValue(Constant.KEY_MVC_VIEW_SUFFIX);
        sb.append("\t<!-- 视图解析器 -->\n");
        sb.append("\t<bean class=\"org.springframework.web.servlet.view.InternalResourceViewResolver\">\n");
        sb.append("\t\t<property name=\"prefix\" value=\""+prefix+"\"></property>\n");
        sb.append("\t\t<property name=\"suffix\" value=\""+suffix+"\"></property>\n");
        sb.append("\t</bean>\n\n");

        sb.append("\t<bean id=\"multipartResolver\"\n");
        sb.append("\t\tclass=\"org.springframework.web.multipart.commons.CommonsMultipartResolver\">\n");
        sb.append("\t\t<property name=\"defaultEncoding\" value=\"utf-8\"></property>\n");
        sb.append("\t\t<property name=\"maxUploadSize\" value=\"10485760000\"></property>\n");
        sb.append("\t\t<property name=\"maxInMemorySize\" value=\"40960\"></property>\n");
        sb.append("\t</bean>\n\n");

        sb.append("\t<!-- 配置拦截器 -->\n");
        sb.append("\t<!-- <mvc:interceptors>\n");
        sb.append("\t\t<mvc:interceptor>\n");
        sb.append("\t\t\t<mvc:mapping path=\"/" + accessBaseUrl + "*\"/>\n");
        sb.append("\t\t\t<bean class=\"" + basePackage + ".interceptor.ManageInterceptor\"/>\n");
        sb.append("\t\t</mvc:interceptor>\n");
        sb.append("\t</mvc:interceptors> -->\n\n");

        sb.append("</beans>");
        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建applicationContext.xml文件
     *
     * @throws Exception
     */
    public static void createApplicationContextFile(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String bizPackageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_BIZ_PATH);
        String utilPackageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);
        String servicePackageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);
        String mapperPackageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_MAPPER_PATH);

        String suffixPath = "applicationContext" + Constant.FILE_SUFFIX_XML;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.resource_spring, suffixPath);
        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        sb.append("<beans xmlns=\"http://www.springframework.org/schema/beans\"\n");
        sb.append("\t\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
        sb.append("\t\txmlns:tx=\"http://www.springframework.org/schema/tx\"\n");
        sb.append("\t\txmlns:aop=\"http://www.springframework.org/schema/aop\"\n");
        sb.append("\t\txmlns:context=\"http://www.springframework.org/schema/context\"\n");
        sb.append("\t\txsi:schemaLocation=\"http://www.springframework.org/schema/beans\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/beans/spring-beans-4.3.xsd\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/tx\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/tx/spring-tx-4.3.xsd\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/aop\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/aop/spring-aop-4.3.xsd\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/context\n");
        sb.append("\t\t\thttp://www.springframework.org/schema/context/spring-context-4.3.xsd\">\n\n");

        sb.append("\t<context:component-scan base-package=\"" + bizPackageName + "\" />\n");
        sb.append("\t<context:component-scan base-package=\"" + servicePackageName + "\" />\n\n");

        sb.append(dataSource(utilPackageName));
        sb.append(sqlSessionFactory(mapperPackageName));
        sb.append(transactionManager(projInfo.getPackageName()));
        sb.append("</beans>\n");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 创建mybatis.xml文件
     *
     * @throws Exception
     */
    public static void createMybatisFile(ProjInfo projInfo) throws Exception {
        String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String entityPackageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_ENTITY_PATH);
        String suffixPath = "mybatis" + Constant.FILE_SUFFIX_XML;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.resource_mybatis, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        sb.append("<!DOCTYPE configuration\n");
        sb.append("PUBLIC \"-//mybatis.org//DTD Config 3.0//EN\"\n");
        sb.append("\"http://mybatis.org/dtd/mybatis-3-config.dtd\">\n");
        sb.append("<configuration>\n");

        sb.append("\t<settings>\n");
        sb.append("\t\t<!-- 打印查询语句 -->\n");
        sb.append("\t\t<setting name=\"logImpl\" value=\"STDOUT_LOGGING\" />\n");
        sb.append("\t\t<!-- 设置缓存 -->\n");
        sb.append("\t\t<setting name=\"cacheEnabled\" value=\"true\"/>\n");
        sb.append("\t\t<!--开启驼峰命名规则自动转换-->\n");
        sb.append("\t\t<setting name=\"mapUnderscoreToCamelCase\" value=\"true\" />\n");
        sb.append("\t</settings>\n\n");
        sb.append("\t<!-- 设置别名 -->\n");
        sb.append("\t<typeAliases>\n");
        sb.append("\t\t<package name=\"" + entityPackageName + "\"/>\n");
        sb.append("\t</typeAliases>\n\n");
        sb.append("</configuration>\n");

        FileUtil.createFile(filePath, sb.toString(), false);
    }

    /**
     * 数据源
     *
     * @return
     */
    private static String dataSource(String utilPackageName) {
        StringBuffer buffer = new StringBuffer();
        String dbPath = PropertiesUtil.getConfigValue(Constant.KEY_RESOURCE_DB);
        buffer.append("\t<!-- 读取db.properties配置文件 -->\n");
        buffer.append("\t<context:property-placeholder location=\"classpath:"+dbPath+"db.properties\"/>\n");

        buffer.append("\t<!-- 配置数据源 -->\n");
        buffer.append("\t<bean id=\"dataSource\" class=\"org.springframework.jdbc.datasource.DriverManagerDataSource\">\n");
        buffer.append("\t\t<property name=\"driverClassName\" value=\"${SQL_DRIVER}\" />\n");
        buffer.append("\t\t<property name=\"url\" value=\"${SQL_URL}\" />\n");
        buffer.append("\t\t<property name=\"username\" value=\"${SQL_USERNAME}\" />\n");
        buffer.append("\t\t<property name=\"password\" value=\"${SQL_PASSWORD}\" />\n");
        buffer.append("\t</bean>\n\n");

        /*buffer.append("\t<!-- 配置动态数据源 -->\n");
        buffer.append("\t<bean id=\"dynamicDataSource\" class=\""+utilPackageName+".DynamicDataSource\">  \n");
        buffer.append("\t\t<property name=\"targetDataSources\">\n");
        buffer.append("\t\t\t<map key-type=\"java.lang.String\">\n");
        buffer.append("\t\t\t\t<entry value-ref=\"dataSource\" key=\"dataSource\" />\n");
        buffer.append("\t\t\t</map>\n");
        buffer.append("\t\t</property>\n");
        buffer.append("\t\t<!-- 默认使用dataSource的数据源 -->\n");
        buffer.append("\t\t<property name=\"defaultTargetDataSource\" ref=\"dataSource\" />\n");
        buffer.append("\t</bean>\n\n");*/
        return buffer.toString();
    }


    /**
     * spring与mybatis的整合，配置sqlSessionFactory
     *
     * @return
     */
    private static String sqlSessionFactory(String mapperPackageName) {
        String mappingsPath = PropertiesUtil.getConfigValue(Constant.KEY_MAPPINGS_PATH);
        String mybatisPath = PropertiesUtil.getConfigValue(Constant.KEY_RESOURCE_MYBATIS);
        
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- spring与mybatis的整合，配置sqlSessionFactory -->\n");
        buffer.append("\t<bean id=\"sqlSessionFactory\" class=\"org.mybatis.spring.SqlSessionFactoryBean\">\n");
        buffer.append("\t\t<property name=\"dataSource\" ref=\"dataSource\" />\n");
        buffer.append("\t\t<property name=\"configLocation\" value=\"classpath:" + mybatisPath + Constant.DEFAULT_MYBATIS_NAME + "\" />\n");
        buffer.append("\t\t<property name=\"mapperLocations\">\n");
        buffer.append("\t\t\t<list>\n");
        buffer.append("\t\t\t\t<value>classpath:" + mappingsPath + "/*.xml</value> \n");
        buffer.append("\t\t\t</list>\n");
        buffer.append("\t\t</property>\n");
        buffer.append("\t</bean>\n\n");

        buffer.append("\t<!--接口映射配置-->\n");
        buffer.append("\t<bean id=\"mapperScannerConfigurer\" class=\"org.mybatis.spring.mapper.MapperScannerConfigurer\">\n");
        buffer.append("\t\t<property name=\"sqlSessionFactoryBeanName\" value=\"sqlSessionFactory\"></property>\n");
        buffer.append("\t\t<property name=\"basePackage\" value=\"" + mapperPackageName + "\"/>\n");
        buffer.append("\t</bean>\n\n");

        buffer.append("\t<!-- 配置 sqlSession-->\n");
        buffer.append("\t<bean id=\"sqlSession\" class=\"org.mybatis.spring.SqlSessionTemplate\" scope=\"prototype\">\n");
        buffer.append("\t\t<constructor-arg ref=\"sqlSessionFactory\"></constructor-arg>\n");
        buffer.append("\t</bean>\n\n");
        return buffer.toString();
    }


    /**
     * 定义事务管理器
     *
     * @return
     */
    private static String transactionManager(String packageName) {
        String basePackage = MyStringUtil.isNotNull(packageName) ? packageName : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String servicePath = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_SERVICE_PATH);
        StringBuffer buffer = new StringBuffer();
        buffer.append("\t<!-- 定义事务管理器 -->\n");
        buffer.append("\t<bean id=\"transactionManager\" class=\"org.springframework.jdbc.datasource.DataSourceTransactionManager\">\n");
        buffer.append("\t\t<property name=\"dataSource\" ref=\"dataSource\" />\n");
        buffer.append("\t\t<property name=\"globalRollbackOnParticipationFailure\" value=\"true\" />\n");
        buffer.append("\t</bean>\n\n");

        buffer.append("\t<!-- 定义事务策略 -->\n");
        buffer.append("\t<tx:advice id=\"txAdvice\" transaction-manager=\"transactionManager\">\n");
        buffer.append("\t\t<tx:attributes>\n");
        buffer.append("\t\t\t<!--所有以select开头的方法都是只读的 -->\n");
        buffer.append("\t\t\t<tx:method name=\"select*\" read-only=\"true\"/>\n");
        buffer.append("\t\t\t<!--配置事务传播性，隔离级别以及超时回滚等问题 -->\n");
        buffer.append("\t\t\t<tx:method name=\"save*\" propagation=\"REQUIRED\" />\n");
        buffer.append("\t\t\t<tx:method name=\"del*\" propagation=\"REQUIRED\" />\n");
        buffer.append("\t\t\t<tx:method name=\"update*\" propagation=\"REQUIRED\" />\n");
        buffer.append("\t\t\t<tx:method name=\"add*\" propagation=\"REQUIRED\" />\n");
        buffer.append("\t\t\t<tx:method name=\"insert*\" propagation=\"REQUIRED\" />\n");
        buffer.append("\t\t\t<tx:method name=\"*\" rollback-for=\"Exception\" />\n");
        buffer.append("\t\t</tx:attributes>\n");
        buffer.append("\t</tx:advice>\n\n");

        buffer.append("\t<!-- 配置切面 -->\n");
        buffer.append("\t<aop:config>\n");
        buffer.append("\t\t<aop:pointcut id=\"myPointcut\"\n");
        buffer.append("\t\t\texpression=\"execution(* " + servicePath + ".*.*(..))\" />\n");
        buffer.append("\t\t<!--将定义好的事务处理策略应用到上述的切入点 -->\n");
        buffer.append("\t\t<aop:advisor advice-ref=\"txAdvice\" pointcut-ref=\"myPointcut\" />\n");
        buffer.append("\t</aop:config>\n\n");

        return buffer.toString();
    }


    /**
     * 创建web.xml文件
     *
     * @throws Exception
     */
    public static void createWebXML(ProjInfo projInfo) throws Exception {
        String suffixPath = PropertiesUtil.getConfigValue(Constant.KEY_WEBXML_PATH) + "web" + Constant.FILE_SUFFIX_XML;
        boolean flag = false;
        String webtypeFlag = PropertiesUtil.getConfigValue(Constant.KEY_WEB_TYPE_FLAG);
        String springPath = PropertiesUtil.getConfigValue(Constant.KEY_RESOURCE_SPRING);
        if ("true".equals(webtypeFlag)) {
            flag = true;
        }
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.webappPath, suffixPath);
        StringBuffer sb = new StringBuffer();

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
        sb.append("<web-app version=\"2.5\"\n");
        sb.append("\txmlns=\"http://java.sun.com/xml/ns/javaee\"\n");
        sb.append("\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
        sb.append("\txsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee\n");
        sb.append("\thttp://java.sun.com/xml/ns/javaee/web-app_2_5.xsd\">\n");

        sb.append("\t<display-name></display-name>\n");
        sb.append("\t<listener>\n");
        sb.append("\t\t<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>\n");
        sb.append("\t</listener>\n");

        sb.append("\t<context-param>\n");
        sb.append("\t\t<param-name>contextConfigLocation</param-name>\n");
        sb.append("\t\t<param-value>classpath:"+springPath+"applicationContext.xml</param-value>\n");
        sb.append("\t</context-param>\n");

        sb.append("\t<servlet>\n");
        sb.append("\t\t<servlet-name>springmvc</servlet-name>\n");
        sb.append("\t\t<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>\n");
        sb.append("\t\t<init-param>\n");
        sb.append("\t\t\t<param-name>contextConfigLocation</param-name>\n");
        sb.append("\t\t\t<param-value>classpath:"+springPath+"spring-mvc.xml</param-value>\n");
        sb.append("\t\t</init-param>\n");
        sb.append("\t\t<load-on-startup>1</load-on-startup>\n");
        sb.append("\t</servlet>\n");

        sb.append("\t<servlet-mapping>\n");
        sb.append("\t\t<servlet-name>springmvc</servlet-name>\n");
        sb.append("\t\t<url-pattern>/</url-pattern>\n");
        sb.append("\t</servlet-mapping>\n");

        sb.append("\t<filter>\n");
        sb.append("\t\t<filter-name>encoding</filter-name>\n");
        sb.append("\t\t<filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>\n");
        sb.append("\t\t<init-param>\n");
        sb.append("\t\t\t<param-name>encoding</param-name>\n");
        sb.append("\t\t\t<param-value>UTF-8</param-value>\n");
        sb.append("\t\t</init-param>\n");
        sb.append("\t</filter>\n");

        sb.append("\t<filter-mapping>\n");
        sb.append("\t\t<filter-name>encoding</filter-name>\n");
        sb.append("\t\t<url-pattern>/*</url-pattern>\n");
        sb.append("\t</filter-mapping>\n");

        sb.append("\t<error-page>\n");
        sb.append("\t\t<error-code>404</error-code>\n");
        sb.append("\t\t<location>/WEB-INF/view/error/error404.jsp</location>\n");
        sb.append("\t</error-page>\n");

        sb.append("\t<error-page>\n");
        sb.append("\t\t<error-code>500</error-code>\n");
        sb.append("\t\t<location>/WEB-INF/view/error/error500.jsp</location>\n");
        sb.append("\t</error-page>\n");

        sb.append("\t<welcome-file-list>\n");
        sb.append("\t\t<welcome-file>index.jsp</welcome-file>\n");
        sb.append("\t</welcome-file-list>\n");

        sb.append("</web-app>\n");

        FileUtil.createFile(filePath, sb.toString(), flag);
    }

    /**
     * 复制pom.xml
     *
     * @param projInfo
     */
    public static void copyTemplatePomXml(ProjInfo projInfo) {
        String filePath = PublicFileUtil.class.getClassLoader().getResource("").getPath();
        try {
            filePath = URLDecoder.decode(filePath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        filePath = filePath.replace("/target/test-classes/", "/target/classes/") + "/template/config/template_pom.xml";
        String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
        String outFilePath = rootDir + projInfo.getProjectName() + "\\pom.xml";

        File file = null;
        String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
        //如果是生成zip
        if(!"true".equals(createZipFlag)){
        	outFilePath = outFilePath.replace(rootDir, "");
        	file = new File(outFilePath);
            if (file.exists()) {
                System.out.println(Constant.warnMsg + outFilePath + " 已存在");
                return;
            }
        }else{
        	file = new File(outFilePath);
        }
        
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        System.out.println(DateUtil.formatDate(new Date(), DateUtil.TIMESTAMP_PATTERN) + " " + outFilePath);
        try {
            FileReader reader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(reader);

            File outFile = new File(outFilePath);
            if (!outFile.getParentFile().exists()) {
                outFile.getParentFile().mkdirs();
            }
            FileWriter writer = new FileWriter(outFile);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                if (line.indexOf("PROJECT_PACKAGE_NAME") != -1) {
                    line = line.replaceAll("PROJECT_PACKAGE_NAME", projInfo.getPackageName());
                } else if (line.indexOf("PROJECT_NAME") != -1) {
                    line = line.replaceAll("PROJECT_NAME", projInfo.getProjectName());
                }
                bufferedWriter.write(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
            writer.close();
            reader.close();
            bufferedReader.close();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 复制.gitignore文件
     *
     * @param projInfo
     * @throws IOException
     */
    public static void copyGitIgnore(ProjInfo projInfo) {
        String filePath = PublicFileUtil.class.getClassLoader().getResource("").getPath();
        try {
            filePath = URLDecoder.decode(filePath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        filePath = filePath.replace("/target/test-classes/", "/target/classes/") + "template/config/.template_gitignore";

        String rootDir = PropertiesUtil.getConfigValue(Constant.KEY_CREATE_PROJ_URL);
        String outFilePath = rootDir + projInfo.getProjectName() + "\\.gitignore";
        
        File file = null;
        String createZipFlag = PropertiesUtil.getConfigValue(Constant.KEY_IS_CREATE_ZIP_FILE);
        //如果是生成zip
        if(!"true".equals(createZipFlag)){
        	outFilePath = outFilePath.replace(rootDir, "");
        	file = new File(outFilePath);
            if (file.exists()) {
                System.out.println(Constant.warnMsg + outFilePath + " 已存在");
                return;
            }
        }else{
        	file = new File(outFilePath);
        }
        
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        System.out.println(DateUtil.formatDate(new Date(), DateUtil.TIMESTAMP_PATTERN) + " " + outFilePath);
        FileUtil.copyFile(filePath, outFilePath);
    }
    
    /**
	 * 创建StringUtil.java 文件
	 */
	public static void createStringUtilFile(ProjInfo projInfo) throws IOException {
		String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "StringUtil" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
		
		StringBuffer sb = new StringBuffer();
		sb.append("package "+packageName+";\n\n");
		
		sb.append("/**\n");
		sb.append(" * 字符串操作类\n");
		sb.append(" * \n");
		sb.append(" * @author "+projInfo.getAuthor()+"\n");
		sb.append(" * @version 1.0\n");
		sb.append(" * @date "+DateUtil.formatDate(new Date(),DateUtil.DATE_PATTERN)+"\n");
		sb.append(" */\n");
		sb.append("public class StringUtil {\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 首字母转小写\n");
		sb.append("\t * @param str\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static String toLowerCaseFirstOne(String str) {\n");
		sb.append("\t\tif (Character.isLowerCase(str.charAt(0))) {\n");
		sb.append("\t\t\treturn str;\n");
		sb.append("\t\t} else {\n");
		sb.append("\t\t\treturn (new StringBuilder())\n");
		sb.append("\t\t\t\t\t.append(Character.toLowerCase(str.charAt(0)))\n");
		sb.append("\t\t\t\t\t.append(str.substring(1)).toString();\n");
		sb.append("\t\t}\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 首字母转大写\n");
		sb.append("\t * @param str\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static String toUpperCaseFirstOne(String str) {\n");
		sb.append("\t\tif (Character.isUpperCase(str.charAt(0))) {\n");
		sb.append("\t\t\treturn str;\n");
		sb.append("\t\t} else {\n");
		sb.append("\t\t\treturn (new StringBuilder())\n");
		sb.append("\t\t\t\t\t.append(Character.toUpperCase(str.charAt(0)))\n");
		sb.append("\t\t\t\t\t.append(str.substring(1)).toString();\n");
		sb.append("\t\t}\n");
		sb.append("\t}\n\n");
		
		
		sb.append("\t/**\n");
		sb.append("\t * 对象判空\n");
		sb.append("\t * @param obj\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(Object obj) {\n");
		sb.append("\t\tif(obj==null || \"\".equals(obj.toString())){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 对象判非空\n");
		sb.append("\t * @param obj\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(Object obj) {\n");
		sb.append("\t\tif(obj!=null && !\"\".equals(obj.toString())){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 字符串判空\n");
		sb.append("\t * @param str\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(String str) {\n");
		sb.append("\t\tif(str==null || \"\".equals(str)){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		
		sb.append("\t/**\n");
		sb.append("\t * 整数判空\n");
		sb.append("\t * @param in\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(Integer in) {\n");
		sb.append("\t\tif(in==null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 长整型判空\n");
		sb.append("\t * @param lon\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(Long lon) {\n");
		sb.append("\t\tif(lon==null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 双精度判空\n");
		sb.append("\t * @param d\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(Double d) {\n");
		sb.append("\t\tif(d==null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 单精度判空\n");
		sb.append("\t * @param f\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(Float f) {\n");
		sb.append("\t\tif(f==null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 字节数组判空\n");
		sb.append("\t * @param bt\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNull(byte[] bt) {\n");
		sb.append("\t\tif(bt==null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 字符串判非空\n");
		sb.append("\t * @param str\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(String str) {\n");
		sb.append("\t\tif(str!=null && !\"\".equals(str)){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 整数判非空\n");
		sb.append("\t * @param in\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(Integer in) {\n");
		sb.append("\t\tif(in!=null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 长整数判非空\n");
		sb.append("\t * @param lon\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(Long lon) {\n");
		sb.append("\t\tif(lon!=null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 双精度非空\n");
		sb.append("\t * @param d\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(Double d) {\n");
		sb.append("\t\tif(d!=null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 单精度非空\n");
		sb.append("\t * @param f\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(Float f) {\n");
		sb.append("\t\tif(f!=null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 字节数组判空\n");
		sb.append("\t * @param bt\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNotNull(byte[] bt) {\n");
		sb.append("\t\tif(bt!=null){\n");
		sb.append("\t\t\treturn true;\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 将路径转成包\n");
		sb.append("\t * @param path\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static String parsePackName(String path){\n");
		sb.append("\t\tStringBuffer sb=new StringBuffer();\n");
		sb.append("\t\tString [] arr=null;\n");
		sb.append("\t\tif(path.indexOf(\"/\")!=-1){\n");
		sb.append("\t\t\tarr=path.split(\"/\");\n");
		sb.append("\t\t}\n");
		sb.append("\t\tif(path.indexOf(\"\\\\\")!=-1){\n");
		sb.append("\t\t\tarr=path.split(\"\\\\\\\\\");\n");
		sb.append("\t\t}\n");
		sb.append("\t\tsb.append(arr[1]);\n");
		sb.append("\t\tfor(int i=2;i<arr.length;i++){\n");
		sb.append("\t\tsb.append(\".\"+arr[i]);\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn sb.toString();\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 字符串转换驼峰命名\n");
		sb.append("\t * @param str\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static String toUnderscoreToCamelCase(String str){\n");
		sb.append("\t\tString camelStr = \"\";\n");
		sb.append("\t\tif(isNotNull(str)){\n");
		sb.append("\t\t\tString [] items = str.split(\"_\");\n");
		sb.append("\t\t\tfor(int i=0;i<items.length;i++){\n");
		sb.append("\t\t\t\tif(i==0){\n");
		sb.append("\t\t\t\t\tcamelStr += toLowerCaseFirstOne(items[i].toLowerCase());\n");
		sb.append("\t\t\t\t}else{\n");
		sb.append("\t\t\t\t\tcamelStr += toUpperCaseFirstOne(items[i].toLowerCase());\n");
		sb.append("\t\t\t\t}\n");
		sb.append("\t\t\t}\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn camelStr;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 字符串转化 添加单引号  例如 a,b,c 转化成 'a','b','c'\n");
		sb.append("\t * @param str\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static String strToCharAndQuot(String str){\n");
		sb.append("\t\tString newStr = \"\";\n");
		sb.append("\t\tif(isNotNull(str)){\n");
		sb.append("\t\t\tString [] array = str.split(\",\");\n");
		sb.append("\t\t\tfor(int i=0;i<array.length;i++){\n");
		sb.append("\t\t\t\tif(i==0){\n");
		sb.append("\t\t\t\t\tnewStr+=\"'\"+array[i]+\"'\";\n");
		sb.append("\t\t\t\t}else{\n");
		sb.append("\t\t\t\t\tnewStr+=\",'\"+array[i]+\"'\";\n");
		sb.append("\t\t\t\t}\n");
		sb.append("\t\t\t}\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn newStr;\n");
		sb.append("\t}\n\n");
		
		sb.append("}");
		
		FileUtil.createFile(filePath, sb.toString(), false);
	}
    
	/**
	 * 创建RegexUtil.java
	 */
	public static void createRegexUtilFile(ProjInfo projInfo) throws IOException {
		
		String basePackage = MyStringUtil.isNotNull(projInfo.getPackageName()) ? projInfo.getPackageName() : PropertiesUtil.getConfigValue(Constant.KEY_DEFAULT_PACKAGE);
        String packageName = basePackage + "." + PropertiesUtil.getConfigValue(Constant.KEY_UTIL_PATH);

        String suffixPath = packageName.replaceAll("\\.", "\\\\") + "\\" + "RegexUtil" + Constant.FILE_SUFFIX_JAVA;
        String filePath = Constant.getFilePath(projInfo.getProjectName(), Constant.PathType.basePath, suffixPath);
		
		StringBuffer sb = new StringBuffer();
		sb.append("package "+packageName+";\n\n");
		
		sb.append("import java.util.regex.Matcher;\n");
		sb.append("import java.util.regex.Pattern;\n\n");
		
		sb.append("/**\n");
		sb.append(" * 正则表达式校验\n");
		sb.append(" * \n");
		sb.append(" * @author "+projInfo.getAuthor()+"\n");
		sb.append(" * @version 1.0\n");
		sb.append(" * @date "+DateUtil.formatDate(new Date(),DateUtil.DATE_PATTERN)+"\n");
		sb.append(" */\n");
		sb.append("public class RegexUtil {\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是手机号吗？\n");
		sb.append("\t *\n");
		sb.append("\t * @param regStr\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isTelephone(String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"^[1]\\\\d{10}\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是邮箱吗？\n");
		sb.append("\t *\n");
		sb.append("\t * @param regStr\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isEmail(String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"\\\\w+@\\\\w+\\\\.\\\\w+\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是身份证号？\n");
		sb.append("\t *\n");
		sb.append("\t * @param regStr\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isIdentityId(String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"^[1-9](\\\\d{13}|\\\\d{16})([a-z]|[A-Z]|[0-9])\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是网址吗？\n");
		sb.append("\t *\n");
		sb.append("\t * @param regStr\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isWebUrl(String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是以某字符串开头，后面接数字的字符串吗？\n");
		sb.append("\t *\n");
		sb.append("\t * @param startStr\n");
		sb.append("\t * @param regStr\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isStartStr(String startStr, String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"^\" + startStr + \"\\\\d+\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是数字或者数字逗号拼接的字符串吗？\n");
		sb.append("\t *\n");
		sb.append("\t * @param regStr 例如 1  或 1,2,3\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isNumAndQuot(String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"^\\\\d+(,\\\\d+)*$\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("\t/**\n");
		sb.append("\t * 是任意字符或者字符按逗号拼接的字符串吗？\n");
		sb.append("\t *\n");
		sb.append("\t * @param regStr 例如 '1'或'31','2v','3a'\n");
		sb.append("\t * @return\n");
		sb.append("\t */\n");
		sb.append("\tpublic static boolean isCharAndQuot(String regStr) {\n");
		sb.append("\t\tif (regStr != null && !\"\".equals(regStr)) {\n");
		sb.append("\t\t\tString reg = \"^'[\\\\w\\\\W]+'(,'[\\\\w\\\\W]+')*$\";\n");
		sb.append("\t\t\tPattern p = Pattern.compile(reg);\n");
		sb.append("\t\t\tMatcher m = p.matcher(regStr);\n");
		sb.append("\t\t\treturn m.matches();\n");
		sb.append("\t\t}\n");
		sb.append("\t\treturn false;\n");
		sb.append("\t}\n\n");
		
		sb.append("}");
		
		FileUtil.createFile(filePath, sb.toString(), false);
	}
}