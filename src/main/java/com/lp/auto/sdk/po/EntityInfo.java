package com.lp.auto.sdk.po;

import com.lp.auto.sdk.utils.MyStringUtil;

import java.util.List;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-06
 **/
public class EntityInfo {

    private String tableName; //表名
    private String tableComment; //注释
    private String primaryKey;//实体主键
    private String tablePrimaryKey; //数据库表主键
    private String primaryKeyType; //实体主键类型
    private List<String> colnames; //实体属性名
    private List<String> tableColnames; //表列名
    private List<String> attrComments; //属性注释
    private List<String> attrTypes; //属性类型

    public List<String> getAttrComments() {
        return attrComments;
    }

    public void setAttrComments(List<String> attrComments) {
        this.attrComments = attrComments;
    }

    public List<String> getAttrTypes() {
        return attrTypes;
    }

    public void setAttrTypes(List<String> attrTypes) {
        this.attrTypes = attrTypes;
    }

    public String getTablePrimaryKey() {
        return tablePrimaryKey;
    }

    public void setTablePrimaryKey(String tablePrimaryKey) {
        this.tablePrimaryKey = tablePrimaryKey;
    }

    public String getEntityName() {
        return MyStringUtil.toUpperCaseFirstOne(MyStringUtil.toUnderscoreToCamelCase(tableName));
    }

    public List<String> getTableColnames() {
        return tableColnames;
    }

    public void setTableColnames(List<String> tableColnames) {
        this.tableColnames = tableColnames;
    }

    public List<String> getColnames() {
        return colnames;
    }

    public void setColnames(List<String> colnames) {
        this.colnames = colnames;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getPrimaryKeyType() {
        return primaryKeyType;
    }

    public void setPrimaryKeyType(String primaryKeyType) {
        this.primaryKeyType = primaryKeyType;
    }


}
