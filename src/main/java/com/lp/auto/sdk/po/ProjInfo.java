package com.lp.auto.sdk.po;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-06-30
 **/
public class ProjInfo {
    private String projectName; //项目名称
    private String packageName; //项目包名 com.logicalthinking.base
    private String author; //开发者名称
    private boolean webType = true; //web.xml 构建标识 默认true

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isWebType() {
        return webType;
    }

    public void setWebType(boolean webType) {
        this.webType = webType;
    }

}
