package com.lp.auto.sdk;

import com.lp.auto.sdk.manager.CreateProjectManager;
import com.lp.auto.sdk.manager.ProjectManager;
import com.lp.auto.sdk.po.Connector;
import com.lp.auto.sdk.po.EntityInfo;
import com.lp.auto.sdk.po.ProjInfo;
import com.lp.auto.sdk.utils.Constant;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-07-14
 **/
public class AutoCodeSDKTest {

    public static void main(String[] args) {
        ProjInfo projInfo = new ProjInfo();
        projInfo.setAuthor("lanping");
        projInfo.setPackageName("com.logicalthinking.eyeglasses");
        projInfo.setProjectName("AR");

        Connector connector = new Connector();
        connector.setDbType(Constant.DB_TYPE_MYSQL);
        connector.setDriver("com.mysql.jdbc.Driver");
        connector.setUrl("jdbc:mysql://58.250.17.31:3345/ar_eyeglasses?useUnicode=true&useSSL=false&characterEncoding=utf-8");
        connector.setUser("root");
        connector.setPassword("0918wqljsw");
//        connector.setUrl("jdbc:mysql://127.0.0.1:3306/derrick_pig?useUnicode=true&useSSL=false&characterEncoding=utf-8");
//        connector.setUser("root");
//        connector.setPassword("root123456");

        ProjectManager projectManager = CreateProjectManager.getProjectManager(connector.getDbType());
        
        try {
        	EntityInfo entityInfo = new EntityInfo();
            entityInfo.setTableName("login_point_info");
            entityInfo.setTableComment("APP登录统计节点信息");
        	
        	projectManager.createFilesByOneTable(connector, projInfo, entityInfo);
        	
        	//projectManager.createFiles(connector, projInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
